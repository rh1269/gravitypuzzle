﻿using PuzzleGameCrossPlatform.Static_Managers;
using PuzzleGameLibrary.Enums;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuzzleGameCrossPlatform.PlatformSpecific
{
    public class WindowsSettingsLoader : ISettingsLoader
    {        
        public void InitializeSettingsManager()
        {
            //Global
            SettingsManager.GlobalTryAnimation = Convert.ToBoolean(ConfigurationManager.AppSettings["Global_TryAnimation"]);
            SettingsManager.GlobalAllowDiagonalSelection = Convert.ToBoolean(ConfigurationManager.AppSettings["Global_AllowDiagonalSelection"]);
            SettingsManager.GlobalAttemptSpeedUp = Convert.ToBoolean(ConfigurationManager.AppSettings["Global_AttemptSpeedUp"]);
            SettingsManager.EnvironmentType = EnvironmentTypeEnum.Desktop;
            

            //Board Settings
            SettingsManager.Board_Xmax = Convert.ToInt32(ConfigurationManager.AppSettings["NumCols"]); //og NumRows
            SettingsManager.Board_Ymax = Convert.ToInt32(ConfigurationManager.AppSettings["NumRows"]); //og NumCols
            SettingsManager.GameBoardStartingX = Convert.ToInt32(ConfigurationManager.AppSettings["GameBoardStartingX"]);
            SettingsManager.GameBoardStartingY = Convert.ToInt32(ConfigurationManager.AppSettings["GameBoardStartingY"]);

            //CountDown Timer Settings
            //SettingsManager.CountDownTimerInitialTime = Convert.ToInt32(ConfigurationManager.AppSettings["CountDownTimerInitialTime"]);
            SettingsManager.BasicCellClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["BasicCellClearTime"]);
            SettingsManager.BonusCellClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["BonusCellClearTime"]);

            //Point Settings
            SettingsManager.BasicPointValue = Convert.ToInt32(ConfigurationManager.AppSettings["BasicPointValue"]);
            SettingsManager.ComboPointValue = Convert.ToInt32(ConfigurationManager.AppSettings["ComboPointValue"]);

            //Time Text Settings
            SettingsManager.TimeTextStartingX = Convert.ToInt32(ConfigurationManager.AppSettings["Text_Time_StartingX"]);
            SettingsManager.TimeTextStartingY = Convert.ToInt32(ConfigurationManager.AppSettings["Text_Time_StartingY"]);

            //Hi Score Text 
            SettingsManager.HiScoreTextStartingX = Convert.ToInt32(ConfigurationManager.AppSettings["HiScoreTextStartingX"]);
            SettingsManager.HiScoreTextStartingY = Convert.ToInt32(ConfigurationManager.AppSettings["HiScoreTextStartingY"]);

            //SCORE Text Settings
            SettingsManager.ScoreTextStartingX = Convert.ToInt32(ConfigurationManager.AppSettings["ScoreTextStartingX"]);
            SettingsManager.ScoreTextStartingY = Convert.ToInt32(ConfigurationManager.AppSettings["ScoreTextStartingY"]);

            //Gravity Text Settings
            SettingsManager.GravityTextStartingX = Convert.ToInt32(ConfigurationManager.AppSettings["GravityTextStartingX"]);
            SettingsManager.GravityTextStartingY = Convert.ToInt32(ConfigurationManager.AppSettings["GravityTextStartingY"]);

            //Mode Select Text Settings
            SettingsManager.ModeSelectTextStartingX = Convert.ToInt32(ConfigurationManager.AppSettings["ModeSelectTextStartingX"]);
            SettingsManager.ModeSelectTextStartingY = Convert.ToInt32(ConfigurationManager.AppSettings["ModeSelectTextStartingY"]);

            //Size of Combo List
            SettingsManager.ComboSize = Convert.ToInt32(ConfigurationManager.AppSettings["ComboSize"]);
            SettingsManager.ComboStartingX = Convert.ToInt32(ConfigurationManager.AppSettings["ComboStartingX"]);
            SettingsManager.ComboStartingY = Convert.ToInt32(ConfigurationManager.AppSettings["ComboStartingY"]);
            SettingsManager.ComboHorizontal = Convert.ToBoolean(ConfigurationManager.AppSettings["ComboIsHorizontal"]);

            //Size of cell
            SettingsManager.CellSize = Convert.ToInt32(ConfigurationManager.AppSettings["CellRectSize"]);
            SettingsManager.CellSpeedPixelPerCycle = Convert.ToInt32(ConfigurationManager.AppSettings["Cell_Speed_PixelPerCycle"]);

            //HighScore Settings File
            SettingsManager.HighScoreFileName = Convert.ToString(ConfigurationManager.AppSettings["HighScoreFileName"]);


            //---------------------------------------------------------------------------------------------------------------

            //Time Settings
            SettingsManager.Time_CountDownTimerInitialTime = Convert.ToInt32(ConfigurationManager.AppSettings["CountDownTimerInitialTime"]); ;
            SettingsManager.Time_SecondsBetweenRepopulation = Convert.ToInt32(ConfigurationManager.AppSettings["Time_SecondsBetweenRepopulation"]);
            SettingsManager.Time_SecondsBetweenGravityChange = Convert.ToInt32(ConfigurationManager.AppSettings["Time_SecondsBetweenGravityChange"]);
            SettingsManager.Time_SecondsBetweenComboChange = Convert.ToInt32(ConfigurationManager.AppSettings["Time_SecondsBetweenComboChange"]);

            SettingsManager.Time_SecondsBetweenSpeedUpAttempt = Convert.ToInt32(ConfigurationManager.AppSettings["Time_SecondsBetweenSpeedUpAttempt"]);
            SettingsManager.Time_SecondsBetweenSpeedDownAttempt = Convert.ToInt32(ConfigurationManager.AppSettings["Time_SecondsBetweenSpeedDownAttempt"]);


            //Post config migration

            //HomeButton
            SettingsManager.BtnHome_startX = Convert.ToInt32(ConfigurationManager.AppSettings["Btn_Home_StartingX"]);
            SettingsManager.BtnHome_startY = Convert.ToInt32(ConfigurationManager.AppSettings["Btn_Home_StartingY"]);
            SettingsManager.BtnHome_width = Convert.ToInt32(ConfigurationManager.AppSettings["Btn_Home_Width"]);
            SettingsManager.BtnHome_height = Convert.ToInt32(ConfigurationManager.AppSettings["Btn_Home_Height"]);
            SettingsManager.BtnHome_startText = ConfigurationManager.AppSettings["Btn_Home_StartingText"];


            //CTBButton
            SettingsManager.BtnCTB_startX = Convert.ToInt32(ConfigurationManager.AppSettings["Btn_CTB_StartingX"]);
            SettingsManager.BtnCTB_startY = Convert.ToInt32(ConfigurationManager.AppSettings["Btn_CTB_StartingY"]);
            SettingsManager.BtnCTB_width = Convert.ToInt32(ConfigurationManager.AppSettings["Btn_CTB_Width"]);
            SettingsManager.BtnCTB_height = Convert.ToInt32(ConfigurationManager.AppSettings["Btn_CTB_Height"]);
            SettingsManager.BtnCTB_startText = ConfigurationManager.AppSettings["Btn_CTB_StartingText"];

            //RTCButton
            SettingsManager.BtnRTC_startX = Convert.ToInt32(ConfigurationManager.AppSettings["Btn_RTC_StartingX"]);
            SettingsManager.BtnRTC_startY = Convert.ToInt32(ConfigurationManager.AppSettings["Btn_RTC_StartingY"]);
            SettingsManager.BtnRTC_width = Convert.ToInt32(ConfigurationManager.AppSettings["Btn_RTC_Width"]);
            SettingsManager.BtnRTC_height = Convert.ToInt32(ConfigurationManager.AppSettings["Btn_RTC_Height"]);
            SettingsManager.BtnRTC_startText = ConfigurationManager.AppSettings["Btn_RTC_StartingText"];

            //Reset Button
            SettingsManager.BtnReset_startX = Convert.ToInt32(ConfigurationManager.AppSettings["Btn_Reset_StartingX"]);
            SettingsManager.BtnReset_startY = Convert.ToInt32(ConfigurationManager.AppSettings["Btn_Reset_StartingY"]);
            SettingsManager.BtnReset_width = Convert.ToInt32(ConfigurationManager.AppSettings["Btn_Reset_Width"]);
            SettingsManager.BtnReset_height = Convert.ToInt32(ConfigurationManager.AppSettings["Btn_Reset_Height"]);
            SettingsManager.BtnReset_startText = ConfigurationManager.AppSettings["Btn_Reset_StartingText"];

            //Reset Button
            SettingsManager.BtnPause_startX = Convert.ToInt32(ConfigurationManager.AppSettings["Btn_Pause_StartingX"]);
            SettingsManager.BtnPause_startY = Convert.ToInt32(ConfigurationManager.AppSettings["Btn_Pause_StartingY"]);
            SettingsManager.BtnPause_width = Convert.ToInt32(ConfigurationManager.AppSettings["Btn_Pause_Width"]);
            SettingsManager.BtnPause_height = Convert.ToInt32(ConfigurationManager.AppSettings["Btn_Pause_Height"]);
            SettingsManager.BtnPause_startText = ConfigurationManager.AppSettings["Btn_Pause_StartingText"];

            //Explosion Settings
            SettingsManager.Explosion_defaultParticleSpeed = Convert.ToInt32(ConfigurationManager.AppSettings["Explosion_defaultParticleSpeed"]);
            SettingsManager.Explosion_defaultParticleSize = Convert.ToInt32(ConfigurationManager.AppSettings["Explosion_defaultParticleSize"]);
            SettingsManager.Explosion_defaultParticleDistanceToTravel = Convert.ToInt32(ConfigurationManager.AppSettings["Explosion_defaultParticleDistanceToTravel"]);

    }
    }
}
