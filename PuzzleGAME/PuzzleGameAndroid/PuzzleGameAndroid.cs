using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using PuzzleGame;
using PuzzleGameAndroid.PlatformSpecific;
using PuzzleGameCrossPlatform.Enums;
using PuzzleGameCrossPlatform.Models;
using PuzzleGameCrossPlatform.Static_Managers;
using System;

namespace PuzzleGameAndroid
{
    #region PuzzleGameCode
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class PuzzleGameAndroid : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        //Buttons
        //ButtonReset resetBtn;
        //ButtonPause pauseBtn;

        //ButtonHome homeBtn;
        //ButtonModeClearTheBoard ctbBtn;
        //ButtonModeRaceTheClock rtcBtn;

        double HeightScaler;
        double WidthScaler;

        public PuzzleGameAndroid()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphics.IsFullScreen = true;

            graphics.SupportedOrientations = DisplayOrientation.Portrait;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            //SETTINGS
            this.IsMouseVisible = true;

            //Mono init
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            var targetHeight = 1800;
            var targetWidth = 1080;

            graphics.PreferredBackBufferWidth = targetWidth;
            graphics.PreferredBackBufferHeight = targetHeight;
          

            //TODO: use this.Content to load your game content here
            //MANAGER INIT
            //Load these first
            AssetManager.Load(Content, graphics.GraphicsDevice);
            //Load Asset Manager first
            SettingsManager.Initialize(new AndroidSettingsLoader(), targetHeight, targetWidth);

            PropertyManager.Initialize();
            PropertyManager.hasBeenReset = false;

            PropertyManager.CurrentGameState = GameState.ModeSelect;
            PropertyManager.CurrentGameMode = GameMode.NotAvailable;
            PropertyManager.LastGameState = PropertyManager.CurrentGameState;

            TimeManager.Initialize(new System.TimeSpan());

            //Initialize the ViewManager with the spritebatch object, from here on out ONLY use the View Manager spritebatch
            ViewManager.Initialize(spriteBatch);

            ////OBJECT INIT
            //resetBtn = new ButtonReset();
            //pauseBtn = new ButtonPause();

            //homeBtn = new ButtonHome();
            //ctbBtn = new ButtonModeClearTheBoard();
            //rtcBtn = new ButtonModeRaceTheClock();

            //LAST THING TO DO
            //GameTimer.Start();
            TimeManager.GameClock.Start();

            // Populate Game Mode specific initialization
            GameModeInitialization();
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                //Exit();
                Game.Activity.MoveTaskToBack(true);

            // TODO: Add your update logic here
            PropertyManager.GameBoard.Update();

            //resetBtn.Update();
            //pauseBtn.Update();
            //homeBtn.Update();
            //ctbBtn.Update();
            //rtcBtn.Update();

            ViewManager.UpdateViewByContext(PropertyManager.CurrentGameState, PropertyManager.CurrentGameMode);

            if (PropertyManager.hasBeenReset == true)
                PuzzleGameReset();

            //Check Rules in rule manager.  
            RuleManager.RunRules();

            //If we are in "Clear The Board" and we want to attempt speed changes
            if (SettingsManager.GlobalAttemptSpeedUp && PropertyManager.CurrentGameMode == GameMode.ClearTheBoard && TimeManager.IsTimeToAttemptSpeedUp())
                TimeManager.SpeedUpRepopulation();

            //If we are in "Race the clock" and we want to attempt speed changes
            if (SettingsManager.GlobalAttemptSpeedUp && PropertyManager.CurrentGameMode == GameMode.RaceTheClock && TimeManager.IsTimeToAttemptSpeedDown())
                TimeManager.DecreaseSpeedRepopulation();

            //If enough time has passed trickle cells down from the top
            if (TimeManager.IsTimeToAttemptRepopulation())
                RuleManager.RepopulateMissingCells(true);

            if (TimeManager.IsTimeToAttemptGravityChange())
                PropertyManager.GravityDirection = PropertyManager.getRandomDirection();

            if (TimeManager.IsTimeToAttemptComboChange())
                PropertyManager.CurrentCombo = new Combo();

            //If there are game mode specific updates that need to be made, do them here.
            if (PropertyManager.CurrentGameState == GameState.InProgress)
                GameModeUpdate();

            //Audio
            HandleAudio2();

            //Update Game Global Game State Properties
            PropertyManager.LastGameState = PropertyManager.CurrentGameState;
            PropertyManager.LastPlayerInDangerState = RuleManager.IsPlayerInDanger(PropertyManager.CurrentGameMode);

            //update last game state
            PropertyManager.LastGameState = PropertyManager.CurrentGameState;

            //Mono - update
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Gray);

            //Instead use the view manager now 
            ViewManager.ServeViewByContext(PropertyManager.CurrentGameState, PropertyManager.CurrentGameMode);

            base.Draw(gameTime);
        }
        

        /// <summary>
        /// Handles the Audio/Sound during the General Game update method - USES SoundEffectInstances instead of Songs and the media player.
        /// </summary>
        /// <remarks> LastGameState is update to match CurrentGameState after this method is called in the main loop</remarks>
        public void HandleAudio2()
        {
            //Audio

            //Pause/Play Audio if there was a Game State change
            if (PropertyManager.LastGameState != PropertyManager.CurrentGameState)
            {
                //If we just start again - start playing normal music
                if (PropertyManager.LastGameState == GameState.ModeSelect && PropertyManager.CurrentGameState == GameState.InProgress)
                {
                    //MediaPlayer.Play(AssetManager.HeavySlow);
                    //MediaPlayer.IsRepeating = true;

                    PropertyManager.CurrentSoundEffectInstance.Play();
                }
                //Otherwise if we just get back into play mode - unpause               
                else if (PropertyManager.CurrentGameState == GameState.InProgress)
                {
                    //MediaPlayer.Resume();

                    PropertyManager.CurrentSoundEffectInstance.Play();

                }
                //If we left play mode - go ahead and pause
                else
                    //MediaPlayer.Pause();
                    PropertyManager.CurrentSoundEffectInstance.Stop();

            }
            else if (PropertyManager.CurrentGameState == GameState.InProgress) // If there wasn't a state change and the game is already running
            {
                var currentPlayerDangerState = RuleManager.IsPlayerInDanger(PropertyManager.CurrentGameMode);

                //If there was a change in the danger state update the song
                if (PropertyManager.LastPlayerInDangerState != currentPlayerDangerState)
                {
                    //Danger
                    if (currentPlayerDangerState == true)
                    {
                        //MediaPlayer.Play(AssetManager.LightFast);
                        //MediaPlayer.IsRepeating = true;

                        PropertyManager.CurrentSoundEffectInstance = AssetManager.LightFastInstance;
                        PropertyManager.CurrentSoundEffectInstance.Play();
                        AssetManager.HeavySlowInstance.Stop();

                    }
                    //Safe
                    else
                    {
                        //MediaPlayer.Play(AssetManager.HeavySlow);
                        //MediaPlayer.IsRepeating = true;

                        PropertyManager.CurrentSoundEffectInstance = AssetManager.HeavySlowInstance;
                        PropertyManager.CurrentSoundEffectInstance.Play();
                        AssetManager.LightFastInstance.Stop();
                    }

                }

            }
        }

        private void PuzzleGameReset()
        {
            //Reset Buttons
            //pauseBtn = new ButtonPause();

            //Reset Managers
            PropertyManager.Initialize();
            PropertyManager.hasBeenReset = false;

            //For new timer we must reset
            TimeManager.Initialize(new System.TimeSpan());
            TimeManager.GameClock.Start();

            GameModeInitialization();
        }

        /// <summary>
        /// Populate Game Mode specific initialization variable or settings
        /// </summary>
        private void GameModeInitialization()
        {
            if (PropertyManager.CurrentGameMode == GameMode.ClearTheBoard)
            {
                //For clear the board, kick off witha  new falling row
            }
            else if (PropertyManager.CurrentGameMode == GameMode.RaceTheClock)
            {
                //For clear the board, kick off with a new falling row
                // TimeManager.CountDownTimer = new System.Timers.Timer(SettingsManager.CountDownTimerInitialTime);
            }
        }

        /// <summary>
        /// GameMode specific update tasks
        /// </summary>
        private void GameModeUpdate()
        {
            //The RTC game mode has a count down timer that is managed here
            if (PropertyManager.CurrentGameMode == GameMode.RaceTheClock)
            {
                TimeManager.UpdateCountDownTime();
            }
        }
    }
    #endregion PuzzleGameCode    
}
