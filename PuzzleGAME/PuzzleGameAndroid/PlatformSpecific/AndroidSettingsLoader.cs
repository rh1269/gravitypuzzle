using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using PuzzleGameCrossPlatform.Static_Managers;
using PuzzleGameLibrary.Enums;

namespace PuzzleGameAndroid.PlatformSpecific
{
    public class AndroidSettingsLoader : ISettingsLoader
    {
        public void InitializeSettingsManager()
        {
            SettingsManager.BasicCellClearTime = Convert.ToInt32("250");

            SettingsManager.BonusCellClearTime = Convert.ToInt32("2000");

            //SettingsManager.CountDownTimerInitialTime = Convert.ToInt32("60");

            SettingsManager.BasicCellClearTime = Convert.ToInt32("250");

            SettingsManager.BonusCellClearTime = Convert.ToInt32("2000");


            //Global Settings-- >

            SettingsManager.GlobalTryAnimation = true;
            SettingsManager.GlobalAllowDiagonalSelection = true;
            SettingsManager.GlobalAttemptSpeedUp = true;
            SettingsManager.EnvironmentType = EnvironmentTypeEnum.Android;



            //Button Settings-- >


            //Reset Btn-- >
            SettingsManager.BtnReset_startX = Convert.ToInt32("450");
            SettingsManager.BtnReset_startY = Convert.ToInt32("1000");
            SettingsManager.BtnReset_width = Convert.ToInt32("128");
            SettingsManager.BtnReset_height = Convert.ToInt32("128");
            SettingsManager.BtnReset_startText = "Reset";

            //Pause Btn-- >
            SettingsManager.BtnPause_startX = Convert.ToInt32("450");
            SettingsManager.BtnPause_startY = Convert.ToInt32("800");
            SettingsManager.BtnPause_width = Convert.ToInt32("128");
            SettingsManager.BtnPause_height = Convert.ToInt32("128");
            SettingsManager.BtnPause_startText = "Pause";

            //Home Btn-- >
            SettingsManager.BtnHome_startX = Convert.ToInt32("385");
            SettingsManager.BtnHome_startY = Convert.ToInt32("1200");
            SettingsManager.BtnHome_width = Convert.ToInt32("256");
            SettingsManager.BtnHome_height = Convert.ToInt32("256");
            SettingsManager.BtnHome_startText = "Home";

            //Clear The Board Btn-- >
            SettingsManager.BtnCTB_startX = Convert.ToInt32("100");
            SettingsManager.BtnCTB_startY = Convert.ToInt32("1000");
            SettingsManager.BtnCTB_width = Convert.ToInt32("256");
            SettingsManager.BtnCTB_height = Convert.ToInt32("256");
            SettingsManager.BtnCTB_startText = "Clear The Board";

            //Race The Clock Btn-- >
            SettingsManager.BtnRTC_startX = Convert.ToInt32("700");
            SettingsManager.BtnRTC_startY = Convert.ToInt32("1000");
            SettingsManager.BtnRTC_width = Convert.ToInt32("256");
            SettingsManager.BtnRTC_height = Convert.ToInt32("256");
            SettingsManager.BtnRTC_startText = "Race The Clock";

            //Cell Settings-- >
            SettingsManager.CellSize = Convert.ToInt32("150");
            SettingsManager.CellSpeedPixelPerCycle = Convert.ToInt32("3");

            //Combo Settings-- >
            SettingsManager.ComboSize = Convert.ToInt32("5");
            SettingsManager.ComboStartingX = Convert.ToInt32("50");
            SettingsManager.ComboStartingY = Convert.ToInt32("20");
            SettingsManager.ComboHorizontal = true;

            //Gameboard Settings-- >
            SettingsManager.GameBoardStartingX = Convert.ToInt32("50");
            SettingsManager.GameBoardStartingY = Convert.ToInt32("250");
            SettingsManager.Board_Ymax = Convert.ToInt32("7"); // Num rows
            SettingsManager.Board_Xmax = Convert.ToInt32("6");  //Num cols

            //Text Settings-- >


            //High Score Display-->
            SettingsManager.HiScoreTextStartingX = Convert.ToInt32("500");
            SettingsManager.HiScoreTextStartingY = Convert.ToInt32("1400");

            //Score Display-- >
            SettingsManager.ScoreTextStartingX = Convert.ToInt32("500");
            SettingsManager.ScoreTextStartingY = Convert.ToInt32("1455");


            //Time Display-- >
            SettingsManager.TimeTextStartingX = Convert.ToInt32("500");
            SettingsManager.TimeTextStartingY = Convert.ToInt32("1515");

            //Gravity Text Display-->
            SettingsManager.GravityTextStartingX = Convert.ToInt32("500");
            SettingsManager.GravityTextStartingY = Convert.ToInt32("1575");

            //Mode Select Text Display-->

            SettingsManager.ModeSelectTextStartingX = Convert.ToInt32("250");
            SettingsManager.ModeSelectTextStartingY = Convert.ToInt32("250");


            //Time Settings-- >
            SettingsManager.Time_CountDownTimerInitialTime = Convert.ToInt32("60");
            SettingsManager.Time_SecondsBetweenRepopulation = Convert.ToInt32("3");
            SettingsManager.Time_SecondsBetweenGravityChange = Convert.ToInt32("20");
            SettingsManager.Time_SecondsBetweenComboChange = Convert.ToInt32("30");
            SettingsManager.Time_SecondsBetweenSpeedUpAttempt = Convert.ToInt32("180");

            //Time Settings - For Clear The Board
            SettingsManager.Time_SecondsBetweenSpeedUpAttempt = Convert.ToInt32("90");
            //Time Settings - For Race The Clock
            SettingsManager.Time_SecondsBetweenSpeedDownAttempt = Convert.ToInt32("20");

            //Rule Settings-- >
            SettingsManager.BasicPointValue = Convert.ToInt32("8");
            SettingsManager.ComboPointValue = Convert.ToInt32("25");


            //HighScoreFile Settings-- >
            SettingsManager.HighScoreFileName = "puzzleHiScore.dat";


            //Explosion Settings-- >
            SettingsManager.Explosion_defaultParticleSpeed = 1;
            SettingsManager.Explosion_defaultParticleSize = 10;
            SettingsManager.Explosion_defaultParticleDistanceToTravel = 150;

        }
    }
}