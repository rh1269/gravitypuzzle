using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Views;

namespace PuzzleGameAndroid
{
    //[Activity(Label = "PuzzleGameAndroid"
    //    , MainLauncher = true
    //    , Icon = "@drawable/icon"
    //    , Theme = "@style/Theme.Splash"
    //    , AlwaysRetainTaskState = true
    //    , LaunchMode = Android.Content.PM.LaunchMode.SingleInstance
    //    , ScreenOrientation = ScreenOrientation.SensorLandscape
    //    , ConfigurationChanges = ConfigChanges.Orientation | ConfigChanges.Keyboard | ConfigChanges.KeyboardHidden | ConfigChanges.ScreenSize)]
    [Activity(Label = "Gravity Frenzy"
       , MainLauncher = true
       , Icon = "@drawable/icon"
       , Theme = "@style/Theme.Splash"
       , AlwaysRetainTaskState = true
       , LaunchMode = Android.Content.PM.LaunchMode.SingleInstance
       , ScreenOrientation = ScreenOrientation.Portrait
       , ConfigurationChanges = ConfigChanges.Orientation | ConfigChanges.Keyboard | ConfigChanges.KeyboardHidden | ConfigChanges.ScreenSize)]
    public class Activity1 : Microsoft.Xna.Framework.AndroidGameActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            var g = new PuzzleGameAndroid();
            SetContentView((View)g.Services.GetService(typeof(View)));
            g.Run();
        }
    }
}

