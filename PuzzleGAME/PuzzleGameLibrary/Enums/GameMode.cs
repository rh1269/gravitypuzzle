﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuzzleGameCrossPlatform.Enums
{
    /// <summary>
    /// Describes the Mode/Game Type the application will run in.  Each mode should describe a different variant with different rules 
    /// </summary>
    public enum GameMode
    {
       Basic = 1, //Current mode the application is in - a debug mode,
       ClearTheBoard = 2, // Keep the Board as clear as possible - A full board means game over
       RaceTheClock = 3,  //Countdown, clearing blocks gives points and seconds, stay alive for as long as possible
       NotAvailable = 4,

    }
}
