﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuzzleGameCrossPlatform.Enums
{
    public enum GameState
    {
        InProgress = 1,
        Paused = 2,
        GameOver = 3,
        ModeSelect = 4

    }
}
