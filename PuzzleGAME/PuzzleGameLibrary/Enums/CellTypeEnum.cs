﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuzzleGameCrossPlatform
{
    public enum CellTypeEnum
    {
        Empty = 0,
        Green = 1,
        Red = 2,
        Blue = 3,
        Orange = 4,
        
        //Expanded
        Diamond = 5,
        Star = 6,
        Skull = 7,

        //Original Debug
        X = 11,
        O = 12,
        Square = 13,
        Triangle = 14,



    }
}
