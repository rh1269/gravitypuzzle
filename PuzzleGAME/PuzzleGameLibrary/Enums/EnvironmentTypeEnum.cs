﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuzzleGameLibrary.Enums
{
    public enum EnvironmentTypeEnum
    {
        Desktop = 1,
        Android = 2
    }
}
