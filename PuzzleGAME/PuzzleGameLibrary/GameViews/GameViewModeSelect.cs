﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using PuzzleGame;
using Microsoft.Xna.Framework;
using PuzzleGameCrossPlatform.Static_Managers;
using PuzzleGameCrossPlatform.Enums;
using PuzzleGameLibrary.Explosion;
using PuzzleGameLibrary.Enums;

namespace PuzzleGameCrossPlatform.GameViews
{
    public class GameViewModeSelect : GameView
    {
        
        public GameViewModeSelect(SpriteBatch spriteBatch) : base(spriteBatch)
        {           
        }

        public override void DisplayView(GameState currentState, GameMode currentMode)
        {

            // PropertyManager.explosionTest.Draw(_spriteBatch);


            //Android specific Settings
            if (SettingsManager.EnvironmentType == EnvironmentTypeEnum.Android)
            {
                PropertyManager.BackgroundSprite.Draw(_spriteBatch, new Vector2(0, 0), SettingsManager.SystemDisplayWidth, SettingsManager.SystemDisplayHeight);
                PropertyManager.TitleSprite.Draw(_spriteBatch, new Vector2(SettingsManager.SystemDisplayWidth/10, 50),SettingsManager.GetScaledWidth(900),SettingsManager.GetScaledHeight(900));// for android
            }

            //Desktop specific Setting
            if(SettingsManager.EnvironmentType == EnvironmentTypeEnum.Desktop)               
            {
                PropertyManager.BackgroundSprite.Draw(_spriteBatch, new Vector2(0, 0), SettingsManager.SystemDisplayWidth , SettingsManager.SystemDisplayHeight );
                PropertyManager.TitleSprite.Draw(_spriteBatch, new Vector2(100, 50), SettingsManager.GetScaledWidth(400), SettingsManager.GetScaledHeight(400));
                _spriteBatch.DrawString(AssetManager.Font, "SELECT A MODE", new Vector2(SettingsManager.ModeSelectTextStartingX, SettingsManager.ModeSelectTextStartingY), Color.White);
              
            }

            _rtcBtn.Draw(_spriteBatch);
            _ctbBtn.Draw(_spriteBatch);

        }

       

    }
}
