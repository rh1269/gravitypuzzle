﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using PuzzleGameCrossPlatform.Enums;
using PuzzleGame;
using Microsoft.Xna.Framework;
using PuzzleGameCrossPlatform.Static_Managers;
using PuzzleGameLibrary.Enums;

namespace PuzzleGameCrossPlatform.GameViews
{
    class GameViewGameOverScreen : GameView
    {
        public GameViewGameOverScreen(SpriteBatch spriteBatch) : base(spriteBatch)
        {
        }

        public override void DisplayView(GameState currentState, GameMode currentMode)
        {

            //Android specific Settings
            if (SettingsManager.EnvironmentType == EnvironmentTypeEnum.Android)
            {
                //Game Over State
                _spriteBatch.DrawString(AssetManager.Font, "GAME OVER", new Vector2(SettingsManager.SystemDisplayWidth / 4, 275), Color.White);

                if (currentMode == GameMode.ClearTheBoard)
                    _spriteBatch.DrawString(AssetManager.Font, "You let the board get full!", new Vector2(SettingsManager.SystemDisplayWidth / 4, 325), Color.White);

                if (currentMode == GameMode.RaceTheClock)
                    _spriteBatch.DrawString(AssetManager.Font, "You ran out of time!", new Vector2(SettingsManager.SystemDisplayWidth / 4, 375), Color.White);

                //Hi Score Display
                _spriteBatch.DrawString(AssetManager.Font, "High Score: " + PropertyManager.HighScore.ToString(), new Vector2(SettingsManager.SystemDisplayWidth / 4, 450), Color.White);

                //SCORE Display
                _spriteBatch.DrawString(AssetManager.Font, "Your Score: " + PropertyManager.Score.ToString(), new Vector2(SettingsManager.SystemDisplayWidth / 4, 500), Color.White);

            }

            //Desktop specific Setting
            if (SettingsManager.EnvironmentType == EnvironmentTypeEnum.Desktop)
            {
                _spriteBatch.DrawString(AssetManager.Font, "GAME OVER", new Vector2(300, 275), Color.White);

                //Hi Score Display
                _spriteBatch.DrawString(AssetManager.Font, "High Score: " + PropertyManager.HighScore.ToString(), new Vector2(SettingsManager.HiScoreTextStartingX, SettingsManager.HiScoreTextStartingY), Color.White);

                //SCORE Display
                _spriteBatch.DrawString(AssetManager.Font, "Score: " + PropertyManager.Score.ToString(), new Vector2(SettingsManager.ScoreTextStartingX, SettingsManager.ScoreTextStartingY), Color.White);

                if (currentMode == GameMode.ClearTheBoard)
                    _spriteBatch.DrawString(AssetManager.Font, "You let the board get full!", new Vector2(300, 225), Color.White);

                if (currentMode == GameMode.RaceTheClock)
                    _spriteBatch.DrawString(AssetManager.Font, "You ran out of time!", new Vector2(300, 225), Color.White);
            }    

          
            //Buttons
            _resetBtn.Draw(_spriteBatch);          
            _homeBtn.Draw(_spriteBatch);
        }

        public override void Update()
        {
            _resetBtn.Update();           
            _homeBtn.Update();            
        }
    }
}
