﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using PuzzleGame;
using PuzzleGameCrossPlatform.Enums;
using Microsoft.Xna.Framework;
using PuzzleGameCrossPlatform.Static_Managers;
using PuzzleGameLibrary.Enums;

namespace PuzzleGameCrossPlatform.GameViews
{
    public class GameViewPlayArea : GameView
    {
        private ButtonPause _gamePlayPauseButton;

        public GameViewPlayArea(SpriteBatch spriteBatch) : base(spriteBatch)
        {
            _gamePlayPauseButton = new ButtonPause(new Rectangle(50, SettingsManager.GetScaledHeight(1400), SettingsManager.GetScaledWidth(256), SettingsManager.GetScaledHeight(256)), "pause");
        }

        public override void DisplayView(GameState currentState, GameMode currentMode)
        {

            //Android specific Settings
            if (SettingsManager.EnvironmentType == EnvironmentTypeEnum.Android)
            {
                PropertyManager.BackgroundSprite.Draw(_spriteBatch, new Vector2(0, 0), SettingsManager.SystemDisplayWidth, SettingsManager.SystemDisplayHeight);
                _gamePlayPauseButton.Draw(_spriteBatch);
            }

            
            //Desktop specific Setting
            if (SettingsManager.EnvironmentType == EnvironmentTypeEnum.Desktop)
            {
                PropertyManager.BackgroundSprite.Draw(_spriteBatch, new Vector2(0, 0), SettingsManager.SystemDisplayWidth, SettingsManager.SystemDisplayHeight);

                //Gravity Direction
                _spriteBatch.DrawString(AssetManager.Font, "Gravity is pointing: " + PropertyManager.GravityDirection.ToString(), new Vector2(SettingsManager.GravityTextStartingX, SettingsManager.GravityTextStartingY), Color.White);
               
                _pauseBtn.Draw(_spriteBatch);
            }



            //Hi Score Display
            _spriteBatch.DrawString(AssetManager.Font, "High Score: " + PropertyManager.HighScore.ToString(), new Vector2(SettingsManager.HiScoreTextStartingX, SettingsManager.HiScoreTextStartingY), Color.White);

            //SCORE Display
            _spriteBatch.DrawString(AssetManager.Font, "Score: " + PropertyManager.Score.ToString(), new Vector2(SettingsManager.ScoreTextStartingX, SettingsManager.ScoreTextStartingY), Color.White);

            //TIME Display
            if (currentMode == GameMode.RaceTheClock)
                _spriteBatch.DrawString(AssetManager.Font, "Time: " + TimeManager.CountDownTime.ToString(@"hh\:mm\:ss"), new Vector2(SettingsManager.TimeTextStartingX, SettingsManager.TimeTextStartingY), Color.White);
            else
                _spriteBatch.DrawString(AssetManager.Font, "Time: " + TimeManager.GameClock.Elapsed.ToString(@"hh\:mm\:ss"), new Vector2(SettingsManager.TimeTextStartingX, SettingsManager.TimeTextStartingY), Color.White);

       
            //Play State

            //Combo Area
            PropertyManager.CurrentCombo.Draw(_spriteBatch);

            //Puzzle Grid
            PropertyManager.GameBoard.Draw(_spriteBatch);
                   
            
            
        }

        public override void Update()
        {
            if (SettingsManager.EnvironmentType == EnvironmentTypeEnum.Android)
            {
                _gamePlayPauseButton.Update();
            }
            else
            {
                _pauseBtn.Update();               
            }
        }
    }
}
