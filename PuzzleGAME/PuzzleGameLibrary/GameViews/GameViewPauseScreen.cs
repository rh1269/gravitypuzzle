﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using PuzzleGameCrossPlatform.Enums;
using PuzzleGame;
using Microsoft.Xna.Framework;
using PuzzleGameCrossPlatform.Static_Managers;
using PuzzleGameLibrary.Enums;

namespace PuzzleGameCrossPlatform.GameViews
{
    public class GameViewPauseScreen : GameView
    {
        public GameViewPauseScreen(SpriteBatch spriteBatch) : base(spriteBatch)
        {
        }

        public override void DisplayView(GameState currentState, GameMode currentMode)
        {

            //Android specific Settings
            if (SettingsManager.EnvironmentType == EnvironmentTypeEnum.Android)
            {
                _spriteBatch.DrawString(AssetManager.Font, "GAME PAUSED", new Vector2(SettingsManager.SystemDisplayWidth/4, 275), Color.White);


                //Hi Score Display
                _spriteBatch.DrawString(AssetManager.Font, "High Score: " + PropertyManager.HighScore.ToString(), new Vector2(SettingsManager.SystemDisplayWidth / 4, 375), Color.White);

                //SCORE Display
                _spriteBatch.DrawString(AssetManager.Font, "Current Score: " + PropertyManager.Score.ToString(), new Vector2(SettingsManager.SystemDisplayWidth / 4, 425), Color.White);
            }

            //Desktop specific Setting
            if (SettingsManager.EnvironmentType == EnvironmentTypeEnum.Desktop)
            {
                _spriteBatch.DrawString(AssetManager.Font, "GAME PAUSED", new Vector2(300, 275), Color.White);

                //Hi Score Display
                _spriteBatch.DrawString(AssetManager.Font, "High Score: " + PropertyManager.HighScore.ToString(), new Vector2(SettingsManager.HiScoreTextStartingX, SettingsManager.HiScoreTextStartingY), Color.White);

                //SCORE Display
                _spriteBatch.DrawString(AssetManager.Font, "Score: " + PropertyManager.Score.ToString(), new Vector2(SettingsManager.ScoreTextStartingX, SettingsManager.ScoreTextStartingY), Color.White);
            }



            //Buttons
            _resetBtn.Draw(_spriteBatch);
            _pauseBtn.Draw(_spriteBatch);
            _homeBtn.Draw(_spriteBatch);


        }
    }
}
