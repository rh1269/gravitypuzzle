﻿using Microsoft.Xna.Framework.Graphics;
using PuzzleGame;
using PuzzleGameCrossPlatform.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuzzleGameCrossPlatform.GameViews
{
    public abstract class GameView
    {
        protected SpriteBatch _spriteBatch { get; set; }

        public ButtonReset _resetBtn { get; private set; }
        public ButtonPause _pauseBtn { get; private set; }
        public ButtonHome _homeBtn { get; private set; }
        public ButtonModeClearTheBoard _ctbBtn { get; private set; }
        public ButtonModeRaceTheClock _rtcBtn { get; private set; }

        public GameView(SpriteBatch spriteBatch)
        {
            _spriteBatch = spriteBatch;

            _resetBtn = new ButtonReset();
            _pauseBtn = new ButtonPause();

            _homeBtn = new ButtonHome();
            _ctbBtn = new ButtonModeClearTheBoard();
            _rtcBtn = new ButtonModeRaceTheClock();
        }

        public abstract void DisplayView(GameState currentState, GameMode currentMode);


        public virtual void Update()
        {
            _resetBtn.Update();
            _pauseBtn.Update();
            _homeBtn.Update();
            _ctbBtn.Update();
            _rtcBtn.Update();            
        }

    }
}
