﻿using Microsoft.Xna.Framework.Graphics;
using PuzzleGameLibrary.Enums;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuzzleGameCrossPlatform.Static_Managers
{
    /// <summary>
    /// This class manages external settings stored in the app.config (or else where depending on the export project)
    /// </summary>
    public static class SettingsManager
    {
        //Global
        public static bool GlobalTryAnimation { get; set; }
        public static bool GlobalAllowDiagonalSelection { get; set; }
        public static bool GlobalAttemptSpeedUp { get; set; }

        public static EnvironmentTypeEnum EnvironmentType { get; set; } 

        //Game Mode Settings
        //public static int CountDownTimerInitialTime { get; set; }
        public static int BasicCellClearTime { get; set; }
        public static int BonusCellClearTime { get; set; }


        //Scaling Values
        public static double _heightScaler { get; set; }  //y
        public static double _widthScaler { get; set; } //x
        public static double _avgScaler { get { return (_heightScaler + _widthScaler) / 2; } }


        //Board Settings
        private static int _Board_Xmax { get; set; }
        private static int _Board_Ymax { get; set; }
        private static int _GameBoardStartingX { get; set; }
        private static int _GameBoardStartingY { get; set; }
        //----------------------------------------------------
        public static int Board_Xmax { get; set; }//{ get { return Convert.ToInt32(_Board_Xmax * _widthScaler); } set { _Board_Xmax = value; } }
        public static int Board_Ymax { get; set; }//{ get { return Convert.ToInt32(_Board_Ymax * _heightScaler); } set { _Board_Ymax = value; } }
        public static int GameBoardStartingX { get { return Convert.ToInt32(_GameBoardStartingX * _widthScaler); } set { _GameBoardStartingX = value; } }
        public static int GameBoardStartingY { get { return Convert.ToInt32(_GameBoardStartingY * _heightScaler); } set { _GameBoardStartingY = value; } }

        //Point Settings
        public static int BasicPointValue { get; set; }
        public static int ComboPointValue { get; set; }

        //Time Text Settings
        private static int _TimeTextStartingX { get; set; }
        private static int _TimeTextStartingY { get; set; }
        //----------------------------------------------------
        public static int TimeTextStartingX { get { return Convert.ToInt32(_TimeTextStartingX * _widthScaler); } set { _TimeTextStartingX = value; } }
        public static int TimeTextStartingY { get { return Convert.ToInt32(_TimeTextStartingY * _heightScaler); } set { _TimeTextStartingY = value; } }

        //Hi Score text settings
        private static int _HiScoreTextStartingX { get; set; }
        private static int _HiScoreTextStartingY { get; set; }
        //----------------------------------------------------
        public static int HiScoreTextStartingX { get { return Convert.ToInt32(_HiScoreTextStartingX * _widthScaler); } set { _HiScoreTextStartingX = value; } }
        public static int HiScoreTextStartingY { get { return Convert.ToInt32(_HiScoreTextStartingY * _heightScaler); } set { _HiScoreTextStartingY = value; } }

        //SCORE Text Settings
        private static int _ScoreTextStartingX { get; set; }
        private static int _ScoreTextStartingY { get; set; }
        //----------------------------------------------------
        public static int ScoreTextStartingX { get { return Convert.ToInt32(_ScoreTextStartingX * _widthScaler); } set { _ScoreTextStartingX = value; } }
        public static int ScoreTextStartingY { get { return Convert.ToInt32(_ScoreTextStartingY * _heightScaler); } set { _ScoreTextStartingY = value; } }

        //Gravity Text Settings
        private static int _GravityTextStartingX { get; set; }
        private static int _GravityTextStartingY { get; set; }
        //----------------------------------------------------
        public static int GravityTextStartingX { get { return Convert.ToInt32(_GravityTextStartingX * _widthScaler); } set { _GravityTextStartingX = value; } }
        public static int GravityTextStartingY { get { return Convert.ToInt32(_GravityTextStartingY * _heightScaler); } set { _GravityTextStartingY = value; } }

        //ModeSelect Text Settings
        private static int _ModeSelectTextStartingX { get; set; }
        private static int _ModeSelectTextStartingY { get; set; }
        //----------------------------------------------------
        public static int ModeSelectTextStartingX { get { return Convert.ToInt32(_ModeSelectTextStartingX * _widthScaler); } set { _ModeSelectTextStartingX = value; } }
        public static int ModeSelectTextStartingY { get { return Convert.ToInt32(_ModeSelectTextStartingY * _heightScaler); } set { _ModeSelectTextStartingY = value; } }

        //Combo List
        //private static int _ComboSize { get; set; }
        private static int _ComboStartingX { get; set; }
        private static int _ComboStartingY { get; set; }
        //----------------------------------------------------
        public static int ComboSize { get; set; }
        public static int ComboStartingX { get { return Convert.ToInt32(_ComboStartingX * _widthScaler); } set { _ComboStartingX = value; } }
        public static int ComboStartingY { get { return Convert.ToInt32(_ComboStartingY * _heightScaler); } set { _ComboStartingY = value; } }
        public static bool ComboHorizontal { get; set; }

        //Cell Settings
        private static int _CellSize { get; set; }
        private static int _CellSpeedPixelPerCycle { get; set; }
        //----------------------------------------------------
        public static int CellSize { get { return Convert.ToInt32(_CellSize * _avgScaler); } set { _CellSize = value; } }
        public static int CellSpeedPixelPerCycle { get { return Convert.ToInt32(_CellSpeedPixelPerCycle * _avgScaler); } set { _CellSpeedPixelPerCycle = value; } }

        //Hi-Score FileName
        public static string HighScoreFileName { get; set; }

        //---------------------------------------------------------------------------------------------------------------
        //Post config migration

        //Time Settings
        public static int Time_CountDownTimerInitialTime { get; set; } 
        public static int Time_SecondsBetweenRepopulation  {get; set;}
        public static int Time_SecondsBetweenGravityChange {get; set;}
        public static int Time_SecondsBetweenComboChange   {get; set;}

        public static int Time_SecondsBetweenSpeedUpAttempt { get; set; }
        public static int Time_SecondsBetweenSpeedDownAttempt { get; set; }

        //HomeButton Config
        private static int _BtnHome_startX { get; set; }
        private static int _BtnHome_startY { get; set; }
        private static int _BtnHome_width { get; set; }
        private static int _BtnHome_height { get; set; }
        //----------------------------------------------------
        public static int BtnHome_startX       { get { return Convert.ToInt32(_BtnHome_startX * _widthScaler); } set { _BtnHome_startX = value; } }
        public static int BtnHome_startY       { get { return Convert.ToInt32(_BtnHome_startY * _heightScaler); } set { _BtnHome_startY = value; } }
        public static int BtnHome_width        { get { return Convert.ToInt32(_BtnHome_width * _widthScaler); } set { _BtnHome_width = value; } }
        public static int BtnHome_height       { get { return Convert.ToInt32(_BtnHome_height * _heightScaler); } set { _BtnHome_height = value; } }
        public static string BtnHome_startText { get; set; }

        //CTB Button Config
        private static int _BtnCTB_startX { get; set; }
        private static int _BtnCTB_startY { get; set; }
        private static int _BtnCTB_width { get; set; }
        private static int _BtnCTB_height { get; set; }
        //----------------------------------------------------
        public static int BtnCTB_startX { get { return Convert.ToInt32(_BtnCTB_startX * _widthScaler); } set { _BtnCTB_startX = value; } }
        public static int BtnCTB_startY { get { return Convert.ToInt32(_BtnCTB_startY * _heightScaler); } set { _BtnCTB_startY = value; } }
        public static int BtnCTB_width  { get { return Convert.ToInt32(_BtnCTB_width * _widthScaler); } set { _BtnCTB_width = value; } }
        public static int BtnCTB_height { get { return Convert.ToInt32(_BtnCTB_height * _heightScaler); } set { _BtnCTB_height = value; } }
        public static string BtnCTB_startText { get; set; }

        //RTC Button Config
        private static int _BtnRTC_startX { get; set; }
        private static int _BtnRTC_startY { get; set; }
        private static int _BtnRTC_width  { get; set; }
        private static int _BtnRTC_height { get; set; }
        //----------------------------------------------------
        public static int BtnRTC_startX { get { return Convert.ToInt32(_BtnRTC_startX * _widthScaler); } set { _BtnRTC_startX = value; } }
        public static int BtnRTC_startY { get { return Convert.ToInt32(_BtnRTC_startY * _heightScaler); } set { _BtnRTC_startY = value; } }
        public static int BtnRTC_width  { get { return Convert.ToInt32(_BtnRTC_width * _widthScaler); } set { _BtnRTC_width = value; } }
        public static int BtnRTC_height { get { return Convert.ToInt32(_BtnRTC_height * _heightScaler); } set { _BtnRTC_height = value; } }
        public static string BtnRTC_startText { get; set; }

        //Reset Button Config
        private static int _BtnReset_startX { get; set; }
        private static int _BtnReset_startY { get; set; }
        private static int _BtnReset_width { get; set; }
        private static int _BtnReset_height { get; set; }
        //----------------------------------------------------
        public static int BtnReset_startX { get { return Convert.ToInt32(_BtnReset_startX * _widthScaler); } set { _BtnReset_startX = value; } }
        public static int BtnReset_startY { get { return Convert.ToInt32(_BtnReset_startY * _heightScaler); } set { _BtnReset_startY = value; } }
        public static int BtnReset_width  { get { return Convert.ToInt32(_BtnReset_width * _widthScaler); } set { _BtnReset_width = value; } }
        public static int BtnReset_height { get { return Convert.ToInt32(_BtnReset_height * _heightScaler); } set { _BtnReset_height = value; } }
        public static string BtnReset_startText { get; set; }

        //Pause Button Config
        private static int _BtnPause_startX { get; set; }
        private static int _BtnPause_startY { get; set; }
        private static int _BtnPause_width { get; set; }
        private static int _BtnPause_height { get; set; }
        //----------------------------------------------------
        public static int BtnPause_startX { get { return Convert.ToInt32(_BtnPause_startX * _widthScaler); } set { _BtnPause_startX = value; } }
        public static int BtnPause_startY { get { return Convert.ToInt32(_BtnPause_startY * _heightScaler); } set { _BtnPause_startY = value; } }
        public static int BtnPause_width  { get { return Convert.ToInt32(_BtnPause_width * _widthScaler); } set { _BtnPause_width = value; } }
        public static int BtnPause_height { get { return Convert.ToInt32(_BtnPause_height * _heightScaler); } set { _BtnPause_height = value; } }
        public static string BtnPause_startText { get; set; }

        //Explosion Settings
        private static int _Explosion_defaultParticleSpeed { get; set; }
        private static int _Explosion_defaultParticleSize { get; set; }
        private static int _Explosion_defaultParticleDistanceToTravel { get; set; }
        //----------------------------------------------------
        public static int Explosion_defaultParticleSpeed { get { return Convert.ToInt32(_Explosion_defaultParticleSpeed * _avgScaler); } set { _Explosion_defaultParticleSpeed = value; } }
        public static int Explosion_defaultParticleSize { get { return Convert.ToInt32(_Explosion_defaultParticleSize * _avgScaler); } set { _Explosion_defaultParticleSize = value; } }
        public static int Explosion_defaultParticleDistanceToTravel { get { return Convert.ToInt32(_Explosion_defaultParticleDistanceToTravel * _avgScaler); } set { _Explosion_defaultParticleDistanceToTravel = value; } }

        public static int SystemDisplayHeight { get; set; }
        public static int SystemDisplayWidth { get; set; }


        public static int GetScaledWidth(int inWidth)
        {
            return Convert.ToInt32(inWidth * _widthScaler);
        }

        public static int GetScaledHeight(int inHeight)
        {
            return Convert.ToInt32(inHeight * _heightScaler);
        }

        public static void Initialize(ISettingsLoader settingsLoader, int targetHeight, int targetWidth)
        {           
            SystemDisplayHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            SystemDisplayWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;

            _heightScaler = Convert.ToDouble(Convert.ToDouble(SystemDisplayHeight) / Convert.ToDouble(targetHeight));
            _widthScaler = Convert.ToDouble(Convert.ToDouble(SystemDisplayWidth) / Convert.ToDouble(targetWidth));           

            settingsLoader.InitializeSettingsManager();
        }

    }
}
