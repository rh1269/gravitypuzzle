﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PuzzleGame.Models;
using System.Configuration;
using PuzzleGameCrossPlatform.Models;
using PuzzleGameCrossPlatform.Static_Managers;
using PuzzleGameCrossPlatform;
using System.IO;
using Microsoft.Xna.Framework.Storage;
using System.Xml.Serialization;
using PuzzleGameCrossPlatform.Enums;

namespace PuzzleGame
{
   public static class PropertyManager
    {
        //public static Player player1 { get; set; }

        //public static Player player2 { get; set; }
        
        //public static Player currentPlayer { get; set; }

        //public static Player winner { get; set; }

       
        ////Point Settings
        //public static int BasicPointValue { get; set; }
        //public static int BonusPointValue { get; set; }


        ////Time Text Settings
        //public static int TimeTextStartingX { get; set; }
        //public static int TimeTextStartingY { get; set; }

        ////SCORE Text Settings
        //public static int ScoreTextStartingX { get; set; }
        //public static int ScoreTextStartingY { get; set; }

        ////Size of Combo List
        //public static int ComboSize { get; set; }


        public static GameState CurrentGameState { get; set; }

        /// <summary>
        /// Current Score/Points the user has accumulated
        /// </summary>
        public static int Score { get; set; }

        public static bool GameInProgress { get; set; }

        public static bool hasBeenReset { get; set; }


        public static List<Cell> SelectedCells { get; set; }


        public static Board GameBoard;

        //public static CellContent[] ComboArray { get; set; }
        public static Combo CurrentCombo { get; set; }

        public static DirectionEnum GravityDirection { get; set; }

        private static Random _random;

        private static int _highScore { get; set; }
        public static int HighScore { get { return _highScore; }
            set {
                var hiScore = new HighScoreData(){ HighScore = value };

                SaveHighScore(hiScore, SettingsManager.HighScoreFileName);

                _highScore = value;
            } }


        public static void Initialize()
        {
            _random = new Random(Guid.NewGuid().GetHashCode());

            //player1 = new Player() { playerNumber = 1, name = "Player 1 (X)" };
            //player2 = new Player() { playerNumber = 2, name = "Player 2 (O)" };

            //currentPlayer = player1;

            //winner = null;


            ////Point Settings
            //BasicPointValue = Convert.ToInt32(ConfigurationManager.AppSettings["BasicPointValue"]);
            //BonusPointValue = Convert.ToInt32(ConfigurationManager.AppSettings["BonusPointValue"]);

            ////Time Text Settings
            //TimeTextStartingX = Convert.ToInt32(ConfigurationManager.AppSettings["Text_Time_StartingX"]);
            //TimeTextStartingY = Convert.ToInt32(ConfigurationManager.AppSettings["Text_Time_StartingY"]);

            ////SCORE Text Settings
            //ScoreTextStartingX = Convert.ToInt32(ConfigurationManager.AppSettings["ScoreTextStartingX"]);
            //ScoreTextStartingY = Convert.ToInt32(ConfigurationManager.AppSettings["ScoreTextStartingY"]);

            ////Size of Combo List
            //ComboSize = Convert.ToInt32(ConfigurationManager.AppSettings["ComboSize"]);


            CurrentGameState = GameState.InProgress;

            GameInProgress = true;

            SelectedCells = new List<Cell>();

            GameBoard = new Board();

            Score = 0;

            CurrentCombo = new Combo();

            // ComboArray = new CellContent[SettingsManager.ComboSize];
            //GetNewCombo();

            GravityDirection = DirectionEnum.Down;

            //highscore section
            var hiScoreObj = LoadHighScore(SettingsManager.HighScoreFileName);
            _highScore = (hiScoreObj != null) ? hiScoreObj.HighScore : 0;
        }


        public static DirectionEnum getRandomDirection()
        {         
            return (DirectionEnum)_random.Next(1, 5);
        }

        //public static void GetNewCombo() {

        //    var newCombo = new CellContent[SettingsManager.ComboSize];

        //    for (int x = 0; x < SettingsManager.ComboSize; x++)
        //        newCombo[x].cellTexture = AssetManager.GetRandomTexture();

        //    ComboArray = newCombo;
        //}

        //public static void updatePlayer()
        //{
        //    if (currentPlayer == player1)
        //        currentPlayer = player2;
        //    else
        //        currentPlayer = player1;
        //}

        #region HighScore
        private static void SaveHighScore(HighScoreData data, string filename)
        {
            // Get the path of the save game
            //string fullpath = Path.Combine(StorageContainer.TitleLocation, filename);

            // Open the file, creating it if necessary
            FileStream stream = File.Open(filename, FileMode.OpenOrCreate);
            try
            {
                // Convert the object to XML data and put it in the stream
                XmlSerializer serializer = new XmlSerializer(typeof(HighScoreData));
                serializer.Serialize(stream, data);
            }
            finally
            {
                // Close the file
                stream.Close();
            }
        }

        public static HighScoreData LoadHighScore(string filename)
        {
            HighScoreData data;

            // Get the path of the save game
            //string fullpath = Path.Combine(StorageContainer.TitleLocation, filename);

            // Open the file
            FileStream stream = File.Open(filename, FileMode.OpenOrCreate,
            FileAccess.Read);
            try
            {

                // Read the data from the file
                XmlSerializer serializer = new XmlSerializer(typeof(HighScoreData));
                data = (HighScoreData)serializer.Deserialize(stream);
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                // Close the file
                stream.Close();
            }

            return (data);
        }

        [Serializable]
        public class HighScoreData
        {
            public int HighScore { get; set; }
        }

        #endregion HighScore
    }
}
