﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using PuzzleGameCrossPlatform;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;

namespace PuzzleGame
{
    /// <summary>
    /// This class manages external assets (i.e. Image, Sound, Video files)
    /// </summary>
    public static class AssetManager
    {
        public static Texture2D X;
        public static Texture2D O;
        public static Texture2D Square;
        public static Texture2D Triangle;

        //Additional Textures
        public static Texture2D Diamond;
        public static Texture2D Star;
        public static Texture2D Skull;

        public static Texture2D ButtonTexture;

        public static Texture2D HighlightTexture;

        //Experimental SPECIAL Combo cell texture
        public static Texture2D specialComboBackgroundTexture;


        public static Texture2D BoardBackgroundColor;

        //Explosion-Particle Textures
        // public static Texture2D particleTextureRed;

        //Button Textures
        public static Texture2D HomeBtnTexture;
        public static Texture2D RaceTheClockBtnTexture;
        public static Texture2D ClearTheBoardBtnTexture;
        public static Texture2D PauseBtnTexture;
        public static Texture2D ResetBtnTexture;
        public static Texture2D PlayButtonTexture;


        public static SpriteFont Font;

        //SpriteSheets
        public static Texture2D Green;  //knife hands
        public static Texture2D Red;  //3 eyes 
        public static Texture2D Blue;  //triangle army
        public static Texture2D Orange; //scary face
        public static Texture2D BackgroundSpriteSheet;
        public static Texture2D TitleSpriteSheet;



        //Sounds
        //public static Song HeavySlow;
        //public static Song LightFast;

        private static SoundEffect HeavySlowSoundEffect;
        private static SoundEffect LightFastSoundEffect;

        public static SoundEffectInstance HeavySlowInstance;
        public static SoundEffectInstance LightFastInstance;

        private static Random _random;
        public static void Load(ContentManager cm, GraphicsDevice gd)
        {

            _random = new Random(Guid.NewGuid().GetHashCode());

            X = cm.Load<Texture2D>("XBlue");
            O = cm.Load<Texture2D>("OPink");
            Square = cm.Load<Texture2D>("SquareOrange");
            Triangle = cm.Load<Texture2D>("TriangleGreen");

            //Additional Textures
            Diamond = cm.Load<Texture2D>("DiamondRed");
            Star = cm.Load<Texture2D>("StarYellow");
            Skull = cm.Load<Texture2D>("SkullBlack");

            ButtonTexture = new Texture2D(gd, 1, 1);
            ButtonTexture.SetData(new Color[] { Color.White });

            HighlightTexture = new Texture2D(gd, 1, 1);
            HighlightTexture.SetData(new Color[] { Color.Yellow });

            //Experimental SPECIAL Combo cell texture
            specialComboBackgroundTexture = new Texture2D(gd, 1, 1);
            specialComboBackgroundTexture.SetData(new Color[] { Color.LightSlateGray });

            BoardBackgroundColor = new Texture2D(gd, 1, 1);
            BoardBackgroundColor.SetData(new Color[] {Color.FromNonPremultiplied(49, 46, 46, 50) });

            //Explosion-Particle Textures
            //  particleTextureRed = new Texture2D(gd, 1, 1);
            // particleTextureRed.SetData(new Color[] { Color.Red });

            //Button Textures
            HomeBtnTexture = cm.Load<Texture2D>("PuzzleHomeIcon");
            RaceTheClockBtnTexture = cm.Load<Texture2D>("PuzzleTextButton_RTC");
            ClearTheBoardBtnTexture = cm.Load<Texture2D>("PuzzleTextButton_CTB");
            PauseBtnTexture = cm.Load<Texture2D>("PauseBtnIcon");
            ResetBtnTexture = cm.Load<Texture2D>("ResetBtnIcon");
            PlayButtonTexture = cm.Load<Texture2D>("PlayButtonIcon");          


            Font = cm.Load<SpriteFont>("BasicFont");

            //SpriteSheets
            Green = cm.Load<Texture2D>("FramePuzzle_GreenKnifeHand50");//2x2 4 total
            Red = cm.Load<Texture2D>("FramePuzzle_RedSpiral50"); //3x3 9 total
            Blue = cm.Load<Texture2D>("FramePuzzle_BlueTriangleArmy50"); // 2x3 6 total
            Orange = cm.Load<Texture2D>("FramePuzzle_OrangeFace50"); //4x4 15 total
            BackgroundSpriteSheet = cm.Load<Texture2D>("BlackHoleBG");
            TitleSpriteSheet = cm.Load<Texture2D>("GravityFrenzyTitle");           


            //Sound
            //HeavySlow = cm.Load<Song>("HeavySlow");
            //LightFast = cm.Load<Song>("LightFast");


            HeavySlowSoundEffect = cm.Load<SoundEffect>("HeavySlowEffect");
            LightFastSoundEffect = cm.Load<SoundEffect>("LightFastEffect");

            HeavySlowInstance = HeavySlowSoundEffect.CreateInstance();
            HeavySlowInstance.IsLooped = true;
            LightFastInstance = LightFastSoundEffect.CreateInstance();
            LightFastInstance.IsLooped = true;
        }

        [Obsolete("I don't think this should be used in most cases, instead create cell content via cell type")]
        public static Texture2D GetRandomCellTexture(int highestNumOfTextures = 5)
        {
            //original
            //var SelectType = (CellTypeEnum)_random.Next(1, 5);

            //Expanded
            //var SelectType = (CellTypeEnum)_random.Next(1, 7);

            var SelectType = (CellTypeEnum)_random.Next(1, highestNumOfTextures);

            switch (SelectType)
            {
               
                case CellTypeEnum.X: return X;
                case CellTypeEnum.O: return O;
                case CellTypeEnum.Square: return Square;
                     default: 
                case CellTypeEnum.Triangle: return Triangle;
                
                //Expanded
                case CellTypeEnum.Diamond: return Diamond;
                case CellTypeEnum.Star: return Star;
                case CellTypeEnum.Skull: return Skull;
            }

        }

    }
}
