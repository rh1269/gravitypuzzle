﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PuzzleGame.Models;
using System.Configuration;
using PuzzleGameCrossPlatform.Models;
using PuzzleGameCrossPlatform.Static_Managers;
using PuzzleGameCrossPlatform;
using System.IO;
//using Microsoft.Xna.Framework.Storage;
using System.Xml.Serialization;
using PuzzleGameCrossPlatform.Enums;
using System.IO.IsolatedStorage;
using PuzzleGameLibrary.Explosion;
using Microsoft.Xna.Framework;
using PuzzleGameLibrary.Models;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace PuzzleGame
{
    /// <summary>
    /// This Manages Global static objects and properties so they are accessible throughout the system and their state is maintained 
    /// </summary>
    public static class PropertyManager
    {
        
        public static GameMode CurrentGameMode { get; set; }       

        public static int BasicCellClearTime { get; set; }
        public static int BonusCellClearTime { get; set; }

        public static GameState CurrentGameState { get; set; }
        public static GameState LastGameState { get; set; }

        /// <summary>
        /// Current Score/Points the user has accumulated
        /// </summary>
        public static int Score { get; set; }

        public static bool hasBeenReset { get; set; }

        public static List<Cell> SelectedCells { get; set; }

        public static Board GameBoard;
       
        public static Combo CurrentCombo { get; set; }

        public static DirectionEnum GravityDirection { get; set; }

        public static AnimatedSprite BackgroundSprite { get; set; }

        public static AnimatedSprite TitleSprite { get; set; }

        private static Random _random;

        //Currently only used for audio and updated in HandleAudio
        public static bool LastPlayerInDangerState { get; set; }

        public static SoundEffectInstance CurrentSoundEffectInstance { get; set; }

     
        private static int _basicHighScore { get; set; }
        private static int _clearTheBoardHighScore { get; set; }
        private static int _raceTheClockHighScore { get; set; }


       


        public static int HighScore
        {
            get
            {
                switch (CurrentGameMode)
                {
                    case GameMode.ClearTheBoard: return _clearTheBoardHighScore;
                    case GameMode.RaceTheClock: return _raceTheClockHighScore;
                    default:
                    case GameMode.Basic: return _basicHighScore;
                }
            }
            set
            {
                var hiScore = new HighScoreData()
                {

                    BasicHighScore = _basicHighScore,
                    ClearTheBoardHighScore = _clearTheBoardHighScore,
                    RaceTheClockHighScore = _raceTheClockHighScore
                };

                switch (CurrentGameMode)
                {
                    case GameMode.ClearTheBoard:
                        hiScore.ClearTheBoardHighScore = value;
                        _clearTheBoardHighScore = value;
                        break;
                    case GameMode.RaceTheClock:
                        hiScore.RaceTheClockHighScore = value;
                        _raceTheClockHighScore = value;
                        break;
                    default:
                    case GameMode.Basic:
                        hiScore.BasicHighScore = value;
                        _basicHighScore = value;
                        break;
                }

                SaveHighScore(hiScore, SettingsManager.HighScoreFileName);
            }
        }


        public static void Initialize()
        {
            _random = new Random(Guid.NewGuid().GetHashCode());


            //This should be temporary eventually allow user to set at runtime
            //CurrentGameMode = (GameMode)Convert.ToInt32(ConfigurationManager.AppSettings["StartInMode"]);

            BasicCellClearTime = SettingsManager.BasicCellClearTime;
            BonusCellClearTime = SettingsManager.BonusCellClearTime;
            
            SelectedCells = new List<Cell>();

            GameBoard = new Board();

            Score = 0;

            CurrentCombo = new Combo();

            GravityDirection = DirectionEnum.Down;

            LastPlayerInDangerState = false;

            BackgroundSprite = new AnimatedSprite(AssetManager.BackgroundSpriteSheet, 2, 2, 3, new TimeSpan(0,0,2));

            TitleSprite = new AnimatedSprite(AssetManager.TitleSpriteSheet, 3, 3, 8, new TimeSpan(0, 0, 1));

            CurrentSoundEffectInstance = AssetManager.HeavySlowInstance;

            //highscore section
            var hiScoreObj = LoadHighScore(SettingsManager.HighScoreFileName);
            _basicHighScore = (hiScoreObj != null) ? hiScoreObj.BasicHighScore : 0;
            _clearTheBoardHighScore = (hiScoreObj != null) ? hiScoreObj.ClearTheBoardHighScore : 0;
            _raceTheClockHighScore = (hiScoreObj != null) ? hiScoreObj.RaceTheClockHighScore : 0;

        }

        /// <summary>
        /// This will return a random Up/Down/Left/Right direction (Does Not include diagonal)
        /// </summary>
        public static DirectionEnum getRandomDirection()
        {  
            return (DirectionEnum)_random.Next(1, 5);
        }

        #region Monogame MULTIPLAT isolated storage HighScore
 
        private static void SaveHighScore(HighScoreData data, string filename)
        {


#if ANDROID
           
             IsolatedStorageFile saveStorage = IsolatedStorageFile.GetUserStoreForApplication();
#else
            IsolatedStorageFile saveStorage = IsolatedStorageFile.GetUserStoreForDomain();
#endif

            IsolatedStorageFileStream fileStream = null;
            fileStream = saveStorage.OpenFile(SettingsManager.HighScoreFileName, FileMode.Create);

            try
            {
                if (fileStream != null)
                {
                    // Convert the object to XML data and put it in the stream
                    XmlSerializer serializer = new XmlSerializer(typeof(HighScoreData));
                    serializer.Serialize(fileStream, data);
                }
            }
            finally
            {
                fileStream.Close();
            }

        }

        public static HighScoreData LoadHighScore(string filename)
        {
         
            HighScoreData data;




#if ANDROID
           
             IsolatedStorageFile saveStorage = IsolatedStorageFile.GetUserStoreForApplication();
#else
            IsolatedStorageFile saveStorage = IsolatedStorageFile.GetUserStoreForDomain();
#endif

            IsolatedStorageFileStream fileStream = null;
            fileStream = saveStorage.OpenFile(SettingsManager.HighScoreFileName, FileMode.OpenOrCreate,
            FileAccess.Read);
            try
            {

                // Read the data from the file
                XmlSerializer serializer = new XmlSerializer(typeof(HighScoreData));
                data = (HighScoreData)serializer.Deserialize(fileStream);
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                // Close the file
                fileStream.Close();
            }

            return (data);
            
        }

        [Serializable]
        public class HighScoreData
        {
            public int BasicHighScore { get; set; }
            public int ClearTheBoardHighScore { get; set; }
            public int RaceTheClockHighScore { get; set; }
        }

        #endregion Monogame MULTIPLAT isolated storage HighScore


        #region HighScore - Windows Only- doesn't work multiplatform
        [Obsolete("Windows Only- doesn't work multiplatform")]
        private static void SaveHighScore_WindowsOnly(HighScoreData_WindowsOnly data, string filename)
        {

            // Open the file, creating it if necessary
            FileStream stream = File.Open(filename, FileMode.OpenOrCreate);
            try
            {
                // Convert the object to XML data and put it in the stream
                XmlSerializer serializer = new XmlSerializer(typeof(HighScoreData));
                serializer.Serialize(stream, data);
            }
            finally
            {
                // Close the file
                stream.Close();
            }
        }

        [Obsolete("Windows Only- doesn't work multiplatform")]
        public static HighScoreData LoadHighScore_WindowsOnly(string filename)
        {
            HighScoreData data;

            // Open the file
            FileStream stream = File.Open(filename, FileMode.OpenOrCreate,
            FileAccess.Read);
            try
            {

                // Read the data from the file
                XmlSerializer serializer = new XmlSerializer(typeof(HighScoreData));
                data = (HighScoreData)serializer.Deserialize(stream);
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                // Close the file
                stream.Close();
            }

            return (data);
        }

        [Serializable]
        public class HighScoreData_WindowsOnly
        {
            public int BasicHighScore { get; set; }
            public int ClearTheBoardHighScore { get; set; }
            public int RaceTheClockHighScore { get; set; }
        }

        #endregion HighScore
    }
}
