﻿using Microsoft.Xna.Framework.Graphics;
using PuzzleGame;
using PuzzleGameCrossPlatform.Enums;
using PuzzleGameCrossPlatform.GameViews;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuzzleGameCrossPlatform.Static_Managers
{
    /// <summary>
    ///  This Manager provides an MVCish View based on a context(state and mode)
    /// </summary>
    public static class ViewManager
    {
        private static SpriteBatch _spriteBatch { get; set; }

        public static void Initialize(SpriteBatch spriteBatch)
        {
            _spriteBatch = spriteBatch;
        }

        /// <summary>
        /// Control Logic that determines what should be drawn on the screen.
        /// </summary>
        /// <param name="currentState"></param>
        /// <param name="currentMode"></param>
        public static void ServeViewByContext(GameState currentState, GameMode currentMode)
        {
            _spriteBatch.Begin();

            var viewToServe = GetCurrentView(currentState);

            if (viewToServe != null)
                viewToServe.DisplayView(currentState, currentMode);

            _spriteBatch.End();
        }

        /// <summary>
        /// Update all sub-component/elements in the current view.
        /// </summary>
        /// <param name="currentState"></param>
        /// <param name="currentMode"></param>
        public static void UpdateViewByContext(GameState currentState, GameMode currentMode)
        {
            _spriteBatch.Begin();

            var viewToServe = GetCurrentView(currentState);

            if (viewToServe != null)
                viewToServe.Update();

            _spriteBatch.End();
        }


        private static GameView GetCurrentView(GameState currentState)
        {
            GameView viewToServe;

            switch (currentState)
            {

                case GameState.ModeSelect:
                    viewToServe = new GameViewModeSelect(_spriteBatch);
                    break;
                case GameState.Paused:
                    viewToServe = new GameViewPauseScreen(_spriteBatch);
                    break;
                case GameState.GameOver:
                    viewToServe = new GameViewGameOverScreen(_spriteBatch);
                    break;
                default:
                case GameState.InProgress:
                    viewToServe = new GameViewPlayArea(_spriteBatch);
                    break;

            }

            return viewToServe;
        }
    }
}
