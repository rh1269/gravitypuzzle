﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PuzzleGame.Models;
using PuzzleGameCrossPlatform.Models;
using PuzzleGameCrossPlatform;
using PuzzleGameCrossPlatform.Static_Managers;
using PuzzleGameCrossPlatform.Enums;

namespace PuzzleGame
{
    /// <summary>
    /// This class manages the games rules
    /// </summary>
    public static class RuleManager
    {

        //Rules To be run during the update phase
        /// <summary>
        /// RunRules() this should be run during the update phase basic checks should be conducted here for end game criteria
        /// </summary>
        public static void RunRules()
        {
            //These rules will run if this is the "Clear the board" Game Mode
            if (PropertyManager.CurrentGameMode == GameMode.ClearTheBoard)
            {
                if (Check_isBoardFull())
                    GameOverProcedure();
            }

            //These rules will run if this is the "Race The Clock" Game Mode
            if (PropertyManager.CurrentGameMode == GameMode.RaceTheClock)
            {
                if (TimeManager.CountDownTime <= new TimeSpan(0))
                    GameOverProcedure();
            }

        }


        /// <summary>
        /// Tasks to run when the Rule manager encounters a situation that will end the game
        /// </summary>
        private static void GameOverProcedure()
        {
            PropertyManager.CurrentGameState = GameState.GameOver;
            TimeManager.GameClock.Stop();
        }

        #region Evaluate selected cells, Alot Points, Clear cells if a win

        //Called during update phase, through gameboard and then through cell.  Triggered if the cell is clicked while in the "SelectedCells" list
        //Used to trigger Evaluation logic to determine if the selected cells are a valid submission
        public static void EvaluateAndClearSelectedCells()
        {
            var sCells = PropertyManager.SelectedCells;

            foreach (var cell in sCells)
            {
                cell.isSelected = false;
            }

            if (isAllSameType(sCells))
            {
                AlotPoints(sCells);
                WinAndClear(sCells);
            }

            if (isComboMatch(sCells))
            {
                AlotPoints(sCells, true);
                WinAndClear(sCells);
            }

            PropertyManager.SelectedCells.Clear();

            //Last step is to repopulate the cells
            RepopulateMissingCells();
        }


        //Return true if all the same type
        private static bool isAllSameType(List<Cell> sCells)
        {
            var firstCellType = sCells.First().Content.CellType;

            foreach (var cell in sCells)
            {
                if (cell.Content.CellType != firstCellType)
                    return false;
            }
            return true;
        }

        //Return true if the selected cells match the cells in the combo list
        private static bool isComboMatch(List<Cell> sCells)
        {
            //Get a copy of the current combo that we can manipulate
            var tempComboCells = PropertyManager.CurrentCombo.DeepCopyCellContent().ToList();

            if (tempComboCells.Count != sCells.Count)
                return false;

            //Go through each selected cell, if this is a match we will tempcombocells list will contain 1 cell entry for each selected
            foreach (var selectedCell in sCells)
            {
                if (tempComboCells.Contains(selectedCell.Content.CellType))
                    tempComboCells.Remove(selectedCell.Content.CellType);
                else
                    return false;
            }

            return true;
        }

        //Use if the selected cells are a valid combo
        //Alot points to player for selected cells, clear out selected cells
        private static void WinAndClear(List<Cell> sCells)
        {
            //AlotPoints(sCells);

            foreach (var cell in sCells)
            {
                PropertyManager.GameBoard.Grid[cell.Xpos, cell.Ypos].ClearCell();//.Content = new CellContent();
            }

        }

        //Update the score based on the selected cells
        private static void AlotPoints(List<Cell> sCells, bool isComboWin = false)
        {
            if (isComboWin)
            {
                foreach (var cell in sCells)
                {
                    PropertyManager.Score += SettingsManager.ComboPointValue;

                    //Add Time back to the count down if this is RaceTheClock
                    if (PropertyManager.CurrentGameMode == GameMode.RaceTheClock)
                    {
                        var ts = new TimeSpan(0, 0, 0, 0, PropertyManager.BonusCellClearTime);
                        TimeManager.IncreaseCountDownTime(ts);
                    }
                }
            }
            else
            {
                foreach (var cell in sCells)
                {
                    PropertyManager.Score += SettingsManager.BasicPointValue;

                    //Add Time back to the count down if this is RaceTheClock
                    if (PropertyManager.CurrentGameMode == GameMode.RaceTheClock)
                    {
                        var ts = new TimeSpan(0, 0, 0, 0, PropertyManager.BasicCellClearTime);
                        TimeManager.IncreaseCountDownTime(ts);
                    }
                }

                PropertyManager.Score += (sCells.Count);
            }

            if (PropertyManager.Score > PropertyManager.HighScore)
                PropertyManager.HighScore = PropertyManager.Score;
        }

        #endregion Evaluate selected cells, Alot Points, Clear cells if a win



        #region Checks
        /// <summary>
        /// Returns true if board has no empty cell types
        /// </summary>
        /// <returns></returns>
        public static bool Check_isBoardFull()
        {
            var Xmax = PropertyManager.GameBoard.Xmax ;
            var Ymax = PropertyManager.GameBoard.Ymax ;

            for (int x = 0; x < Xmax; x++)
            {
                for (int y = 0; y < Ymax; y++)
                {
                    if (PropertyManager.GameBoard.Grid[x, y].Content.CellType == CellTypeEnum.Empty)
                    {
                        //If the cell is empty then the board is no full return false
                        return false;
                    }
                }
            }

            // If we get here the board is full
            return true;
        }

        /// <summary>
        /// Determines if the player is in danger (i.e. if the player is about to lose the game) based on the game type
        /// </summary>
        /// <returns></returns>
        public static bool IsPlayerInDanger(GameMode currentGameMode)
        {          
            //If we have 80% of the boards cells filled, we are in danger for the Clear the board mode
            if (currentGameMode == GameMode.ClearTheBoard)
            {
                int totalPossibleCells = PropertyManager.GameBoard.Xmax * PropertyManager.GameBoard.Ymax;
                int dangerLvl80 = (int)(.8 * totalPossibleCells);

                if (PropertyManager.GameBoard.GetNumberOfPopulatedCells() >= dangerLvl80)
                    return true;
            }

            //If we have 5 seconds or less left in Race The Clock, we are in danger
            if (currentGameMode == GameMode.RaceTheClock)
            {
                var fiveSeconds = new TimeSpan(0, 0, 5);
                if (TimeManager.CountDownTime <= fiveSeconds)
                    return true;
            }

            return false;
        }

        #endregion Checks


        //Repopulations Rules
        //--------------------------------------------------------------------------------------------------------


        //Repopulate Missing/Empty cells
        //This method calls one of the triggers one of the different fill methods
        public static void RepopulateMissingCells(bool populateFirst = false)
        {
            //RepopulateBasicFill();

            switch (PropertyManager.GravityDirection)
            {
                default:
                case DirectionEnum.Down:
                    if (populateFirst)
                        PopulateTopFallToBottom();
                    else
                        FallToBottom();
                    break;
                case DirectionEnum.Up:
                    if (populateFirst)
                        PopulateBottomFallToTop();
                    else
                        FallToTop();
                    break;
                case DirectionEnum.Left:
                    if (populateFirst)
                        PopulateRightFallToLeft();
                    else
                        FallToLeft();
                    break;
                case DirectionEnum.Right:
                    if (populateFirst)
                        PopulateLeftFallToRight();
                    else
                        FallToRight();
                    break;

                    //default:
                    //                    if (populateFirst)
                    //                        PopulateTopFallToBottom();
                    //                    else
                    //                        FallToBottom();
                    //                        break;
            }

        }


        #region Repopulation Helper Methods

        //Returns true if there is an emty cell in the row
        private static bool DoesRowHaveAnEmpty(int rowPos)
        {
            var Xmax = PropertyManager.GameBoard.Xmax - 1;

            var targetY = rowPos;

            for (int x = 0; x < Xmax; x++)
            {
                if (PropertyManager.GameBoard.Grid[x, targetY].Content.CellType != CellTypeEnum.Empty)
                    return true;
            }

            return false;
        }

        //Returns true if there is an emty cell in the column
        private static bool DoesColumnHaveAnEmpty(int colPos)
        {
            var Ymax = PropertyManager.GameBoard.Ymax - 1;

            var targetX = colPos;

            for (int y = 0; y < Ymax; y++)
            {
                if (PropertyManager.GameBoard.Grid[targetX, y].Content.CellType != CellTypeEnum.Empty)
                    return true;
            }

            return false;
        }


        //Fill Any Empty cells in the target row
        private static void FillTargetRow(int rowPos)
        {
            var Xmax = PropertyManager.GameBoard.Xmax;

            var targetY = rowPos;

            for (int x = 0; x < Xmax; x++)
            {
                if (PropertyManager.GameBoard.Grid[x, targetY].Content.CellType == CellTypeEnum.Empty)
                {
                    PropertyManager.GameBoard.Grid[x, targetY].Content = new CellContent().PopulateWithRandom(); 
                }
            }
        }


        //Fill Any Empty cells in the target row
        private static void FillTargetCol(int colPos)
        {
            var Ymax = PropertyManager.GameBoard.Ymax;

            var targetx = colPos;

            for (int y = 0; y < Ymax; y++)
            {
                if (PropertyManager.GameBoard.Grid[targetx, y].Content.CellType == CellTypeEnum.Empty)
                {
                    PropertyManager.GameBoard.Grid[targetx, y].Content = new CellContent().PopulateWithRandom();
                }
            }
        }
        #endregion Repopulation Helper Methods

        #region Repopulate with Basic fill (All cells no direction)
        //Basic Fill
        private static void RepopulateBasicFill()
        {
            foreach (var cell in PropertyManager.GameBoard.Grid)
            {
                if (cell.Content.CellType == CellTypeEnum.Empty)
                    cell.Content = new CellContent().PopulateWithRandom();
            }
        }
        #endregion Repopulate with Basic fill (All cells no direction)

        #region Repopulate with a Fill From Top to Bottom


        //Fills empty cells in the top row and then initiates fill down
        public static void PopulateTopFallToBottom()
        {
            FillTargetRow(0);
            FallToBottom();
        }


        //Cells fall down | empty cells filled in with content from the cell above
        private static void FallToBottom()
        {
            var Xmax = PropertyManager.GameBoard.Xmax - 1;
            var Ymax = PropertyManager.GameBoard.Ymax - 1;

            //Go through Grid, right to left and bottom to top
            for (int x = Xmax; x > -1; x--)
            {
                int y = Ymax;

                while (y > 0)
                {
                    var currentCell = PropertyManager.GameBoard.Grid[x, y];

                    //if the current cell is empty, replace it with the content above
                    if (currentCell.Content.CellType == CellTypeEnum.Empty && y > 0)
                    {
                        var aboveCell = PropertyManager.GameBoard.Grid[x, y - 1];



                        if (SettingsManager.GlobalTryAnimation)
                            //Animation Attempt
                            currentCell.assignDragCell(aboveCell);
                        else
                            //Before animation attempt
                            currentCell.Content = aboveCell.Content;


                        aboveCell.Content = new CellContent();
                    }

                    y--;
                }

                //If the column has gaps, run it again (until it is filled properly)
                if (!haveCellsFallenToBottom(x, Ymax))
                    x++;
            }



        }

        //Returns true if all populated cells have fallen to the bottom, false if there are gaps in the column
        private static bool haveCellsFallenToBottom(int inX, int maxY)
        {
            CellTypeEnum lastType = PropertyManager.GameBoard.Grid[inX, maxY].Content.CellType;

            for (int y = maxY; y > -1; y--)
            {
                var currentType = PropertyManager.GameBoard.Grid[inX, y].Content.CellType;

                if (lastType == CellTypeEnum.Empty && currentType != CellTypeEnum.Empty)
                    return false;

                lastType = currentType;
            }
            return true;
        }

        #endregion Repopulate with a Fill From Top to Bottom

        #region Repopulate with a Fill From Bottom to Top
        //Fills empty cells in the top row and then initiates fill down
        public static void PopulateBottomFallToTop()
        {
            FillTargetRow(PropertyManager.GameBoard.Ymax - 1);
            FallToTop();
        }


        //Cells fall down | empty cells filled in with content from the cell above
        private static void FallToTop()
        {
            var Xmax = PropertyManager.GameBoard.Xmax - 1;
            var Ymax = PropertyManager.GameBoard.Ymax - 1;

            //Go through Grid, right to left and top to bottom
            for (int x = Xmax; x > -1; x--)
            {
                int y = 0;

                while (y < Ymax)
                {
                    var currentCell = PropertyManager.GameBoard.Grid[x, y];

                    //if the current cell is empty, replace it with the content above
                    if (currentCell.Content.CellType == CellTypeEnum.Empty && y < Ymax)
                    {
                        var belowCell = PropertyManager.GameBoard.Grid[x, y + 1];

                        if (SettingsManager.GlobalTryAnimation)
                            //Animation Attempt
                            currentCell.assignDragCell(belowCell);
                        else
                            //Before animation attempt
                            currentCell.Content = belowCell.Content;

                        belowCell.Content = new CellContent();
                    }

                    y++;
                }

                //If the column has gaps, run it again (until it is filled properly)
                if (!haveCellsFallenToTop(x, Ymax))
                    x++;
            }



        }

        //Returns true if all populated cells have fallen to the Top, false if there are gaps in the column
        private static bool haveCellsFallenToTop(int inX, int maxY)
        {
            CellTypeEnum lastType = PropertyManager.GameBoard.Grid[inX, 0].Content.CellType;

            for (int y = 0; y < maxY; y++)
            {
                var currentType = PropertyManager.GameBoard.Grid[inX, y].Content.CellType;

                if (lastType == CellTypeEnum.Empty && currentType != CellTypeEnum.Empty)
                    return false;

                lastType = currentType;
            }
            return true;
        }
        #endregion Repopulate with a Fill From Bottom to Top

        #region Repopulate with a Fill From Left to Right
        //Fills empty cells in the first column and then initiates fill left to right
        public static void PopulateLeftFallToRight()
        {
            FillTargetCol(0);
            FallToRight();
        }


        //Cells fall down | empty cells filled in with content from the cell above
        private static void FallToRight()
        {
            var Xmax = PropertyManager.GameBoard.Xmax - 1;
            var Ymax = PropertyManager.GameBoard.Ymax - 1;

            //Go through Grid, bottom to top , left to right 
            for (int y = Ymax; y > -1; y--)
            {
                int x = Xmax;

                while (x > 0)
                {
                    var currentCell = PropertyManager.GameBoard.Grid[x, y];

                    //if the current cell is empty, replace it with the content to the left
                    if (currentCell.Content.CellType == CellTypeEnum.Empty && x > 0)
                    {
                        var leftCell = PropertyManager.GameBoard.Grid[x - 1, y];

                        if (SettingsManager.GlobalTryAnimation)
                            //Animation Attempt
                            currentCell.assignDragCell(leftCell);
                        else
                            //Before animation attempt
                            currentCell.Content = leftCell.Content;


                        leftCell.Content = new CellContent();
                    }

                    x--;
                }

                //If the column has gaps, run it again (until it is filled properly)
                if (!haveCellsFallenToRight(y, Xmax))
                    y++;
            }

        }

        //Returns true if all populated cells have fallen to the Right
        private static bool haveCellsFallenToRight(int inY, int maxX)
        {
            CellTypeEnum lastType = PropertyManager.GameBoard.Grid[maxX, inY].Content.CellType;

            for (int x = maxX; x > -1; x--)
            {
                var currentType = PropertyManager.GameBoard.Grid[x, inY].Content.CellType;

                if (lastType == CellTypeEnum.Empty && currentType != CellTypeEnum.Empty)
                    return false;

                lastType = currentType;
            }
            return true;
        }


        #endregion Repopulate with a Fill From Left to Right

        #region Repopulate with a Fill From Right to Left
        //Fills empty cells in the first column and then initiates fill left to right
        public static void PopulateRightFallToLeft()
        {
            FillTargetCol(PropertyManager.GameBoard.Xmax - 1);
            FallToLeft();
        }

        //Cells fall down | empty cells filled in with content from cell to the right
        private static void FallToLeft()
        {
            var Xmax = PropertyManager.GameBoard.Xmax - 1;
            var Ymax = PropertyManager.GameBoard.Ymax - 1;

            //Go through Grid, bottom to top , left to right 
            for (int y = Ymax; y > -1; y--)
            {
                int x = 0;

                while (x < Xmax)
                {
                    var currentCell = PropertyManager.GameBoard.Grid[x, y];

                    //if the current cell is empty, replace it with the content to the right
                    if (currentCell.Content.CellType == CellTypeEnum.Empty && x < Xmax)
                    {
                        var rightCell = PropertyManager.GameBoard.Grid[x + 1, y];

                        if (SettingsManager.GlobalTryAnimation)
                            //Animation Attempt
                            currentCell.assignDragCell(rightCell);
                        else
                            //Before animation attempt
                            currentCell.Content = rightCell.Content;


                        rightCell.Content = new CellContent();
                    }

                    x++;
                }

                //If the column has gaps, run it again (until it is filled properly)
                if (!haveCellsFallenToLeft(y, Xmax))
                    y++;
            }

        }

        //Returns true if all populated cells have fallen to the Right
        private static bool haveCellsFallenToLeft(int inY, int maxX)
        {
            CellTypeEnum lastType = PropertyManager.GameBoard.Grid[0, inY].Content.CellType;

            for (int x = 0; x < maxX; x++)
            {
                var currentType = PropertyManager.GameBoard.Grid[x, inY].Content.CellType;

                if (lastType == CellTypeEnum.Empty && currentType != CellTypeEnum.Empty)
                    return false;

                lastType = currentType;
            }
            return true;
        }
        #endregion Repopulate with a Fill From Right to Left


       /// <summary>
       /// Determine how which cell textures we should pick from based on the current score
       /// </summary>
       /// <returns></returns>
        private static int GetNumberOfTexturesToSelectFrom()
        {

            if (PropertyManager.Score > 2000)
                return 6;
            if (PropertyManager.Score > 1000)
                return 5;

            return 4;

        }

        [Obsolete("Not Used currently")]
        private static bool isCellsAboveEmpty(int inX, int inY)
        {
            for (int y = inY - 1; y > 0; y--)
            {
                if (PropertyManager.GameBoard.Grid[inX, y].Content.CellType != CellTypeEnum.Empty)
                    return false;

            }
            return true;
        }

    }
}
