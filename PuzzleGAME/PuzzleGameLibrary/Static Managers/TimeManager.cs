﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PuzzleGame.Models;
using System.Configuration;
using System.Diagnostics;
using PuzzleGameCrossPlatform.Static_Managers;

namespace PuzzleGame
{
    /// <summary>
    /// This class keeps an eye on the timer
    /// </summary>
    public static class TimeManager
    {
        //Measures time elapsed since start of the game session
        public static Stopwatch GameClock;

        public static TimeSpan CountDownTime { get; set; }
        private static TimeSpan LastTimeCountDownChecked { get; set; }

        private static TimeSpan LastTimeBoardRepopulated { get; set; }
        private static TimeSpan TimeBetweenPopulationAttempts { get; set; }

        private static TimeSpan LastTimeGravityChanged { get; set; }
        private static TimeSpan TimeBetweenGravityChangeAttempts { get; set; }


        private static TimeSpan LastTimeComboChanged { get; set; }
        private static TimeSpan TimeBetweenComboChangeAttempts { get; set; }

        private static TimeSpan LastTimeSpeedUpAttempted { get; set; }
        private static TimeSpan TimeBetweenSpeedUpAttempts { get; set; }


        private static TimeSpan LastTimeSpeedDownAttempted { get; set; }
        private static TimeSpan TimeBetweenSpeedDownAttempts { get; set; }


        public static void Initialize(TimeSpan startingGameTime)
        {

            GameClock = new Stopwatch();

            //Setup countdown
            var seconds = SettingsManager.Time_CountDownTimerInitialTime;
            CountDownTime = new TimeSpan(0, 0, seconds);
            LastTimeCountDownChecked = startingGameTime;


            var secondsBetweenRepopulation = SettingsManager.Time_SecondsBetweenRepopulation;

            var secondsBetweenGravityChange = SettingsManager.Time_SecondsBetweenGravityChange;

            var secondsBetweenComboChange = SettingsManager.Time_SecondsBetweenComboChange;

            var secondsBetweenSpeedUpAttempt = SettingsManager.Time_SecondsBetweenSpeedUpAttempt;

            var secondsBetweenSpeedDownAttempt = SettingsManager.Time_SecondsBetweenSpeedDownAttempt;

            //Repopulation time
            LastTimeBoardRepopulated = startingGameTime;
            TimeBetweenPopulationAttempts = new TimeSpan(0, 0, secondsBetweenRepopulation);

            //Gravity Changed Time
            LastTimeGravityChanged = startingGameTime;
            TimeBetweenGravityChangeAttempts = new TimeSpan(0, 0, secondsBetweenGravityChange);

            //Comobo Changed Time
            LastTimeComboChanged = startingGameTime;
            TimeBetweenComboChangeAttempts = new TimeSpan(0, 0, secondsBetweenComboChange);


            //Speed Up Attempts
            LastTimeSpeedUpAttempted = startingGameTime;
            TimeBetweenSpeedUpAttempts = new TimeSpan(0, 0, secondsBetweenSpeedUpAttempt);

            //Speed Down Attempts
            LastTimeSpeedDownAttempted = startingGameTime;
            TimeBetweenSpeedDownAttempts = new TimeSpan(0, 0, secondsBetweenSpeedDownAttempt);

        }

      //Repopulation
        public static bool IsTimeToAttemptRepopulation()
        {
            return IsTimeToAttemptRepopulation(GameClock.Elapsed);
        }

        public static bool IsTimeToAttemptRepopulation(TimeSpan currentGameTime)
        {
            if (currentGameTime > (LastTimeBoardRepopulated + TimeBetweenPopulationAttempts))
            {
                LastTimeBoardRepopulated = currentGameTime;
                return true;
            }

            return false;
        }
             

        //Gravity
        public static bool IsTimeToAttemptGravityChange()
        {
            return IsTimeToAttemptGravityChange(GameClock.Elapsed);
        }

        public static bool IsTimeToAttemptGravityChange(TimeSpan currentGameTime)
        {
            if (currentGameTime > (LastTimeGravityChanged + TimeBetweenGravityChangeAttempts))
            {
                LastTimeGravityChanged = currentGameTime;
                return true;
            }

            return false;
        }


        //Combo
        public static bool IsTimeToAttemptComboChange()
        {
            return IsTimeToAttemptComboChange(GameClock.Elapsed);
        }


        public static bool IsTimeToAttemptComboChange(TimeSpan currentGameTime)
        {
            if (currentGameTime > (LastTimeComboChanged + TimeBetweenComboChangeAttempts))
            {
                LastTimeComboChanged = currentGameTime;
                return true;
            }

            return false;
        }

        //Speed Up (Clear the Board)
        public static bool IsTimeToAttemptSpeedUp()
        {
            return IsTimeToAttemptSpeedUp(GameClock.Elapsed);
        }
        
        public static bool IsTimeToAttemptSpeedUp(TimeSpan currentGameTime)
        {
            if (currentGameTime > (LastTimeSpeedUpAttempted + TimeBetweenSpeedUpAttempts))
            {
                LastTimeSpeedUpAttempted = currentGameTime;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Make the rate that cells fall faster (Clear the Board)
        /// </summary>
        public static void SpeedUpRepopulation()
        {
            if (TimeBetweenPopulationAttempts.Seconds > 1)
                TimeBetweenPopulationAttempts = TimeBetweenPopulationAttempts.Subtract(TimeSpan.FromSeconds(1));
        }



         
        //Speed Down (Race The Clock)
        public static bool IsTimeToAttemptSpeedDown()
        {
            return IsTimeToAttemptSpeedDown(GameClock.Elapsed);
        }

        public static bool IsTimeToAttemptSpeedDown(TimeSpan currentGameTime)
        {
            if (currentGameTime > (LastTimeSpeedDownAttempted + TimeBetweenSpeedDownAttempts))
            {
                LastTimeSpeedDownAttempted = currentGameTime;
                return true;
            }

            return false;
        }
        /// <summary>
        /// Make the rate that cells fall slower (Race The Clock)
        /// </summary>
        public static void DecreaseSpeedRepopulation()
        {
            TimeBetweenPopulationAttempts = TimeBetweenPopulationAttempts.Add(TimeSpan.FromSeconds(1));
        }


              
        /// <summary>
        /// Updates the countdown time
        /// </summary>
        public static void UpdateCountDownTime()
        {
            UpdateCountDownTime(GameClock.Elapsed);
        }

        public static void UpdateCountDownTime(TimeSpan currentGameTime)
        {
            var timeDiff = currentGameTime - LastTimeCountDownChecked;

            LastTimeCountDownChecked = currentGameTime;

            CountDownTime = CountDownTime - timeDiff;
        }

        public static void IncreaseCountDownTime(TimeSpan timeToIncreaseBy)
        {
            CountDownTime = CountDownTime.Add(timeToIncreaseBy);
        }

    }
}
