﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuzzleGameCrossPlatform.Static_Managers
{
    /// <summary>
    /// Interface for Classes that load Settings into the project.  There will be a different implementation for each export project. (i.e. Android project, windows project, ios project.)
    /// </summary>
    public interface ISettingsLoader
    {
        //string GetValue(string key);

        void InitializeSettingsManager();

    }
}
