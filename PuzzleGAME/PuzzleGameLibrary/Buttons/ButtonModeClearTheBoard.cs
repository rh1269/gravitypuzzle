﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Configuration;
using PuzzleGameCrossPlatform.Enums;
using PuzzleGameCrossPlatform.Static_Managers;
using Microsoft.Xna.Framework.Input.Touch;

namespace PuzzleGame
{
    public class ButtonModeClearTheBoard : ButtonForGames
    {

        public ButtonModeClearTheBoard()
        {
            Initialize();
        }

        public override void Initialize()
        {
            //Settings with Image
            ButtonTexture = AssetManager.ClearTheBoardBtnTexture;

            var startX = SettingsManager.BtnCTB_startX;
            var startY = SettingsManager.BtnCTB_startY;
            var width = SettingsManager.BtnCTB_width;
            var height = SettingsManager.BtnCTB_height;
            var startText = SettingsManager.BtnCTB_startText;

            Rect = new Rectangle() { X = startX, Y = startY, Width = width, Height = height };
            displayText = startText;
        }

        public override void Update()
        {
            if (PropertyManager.CurrentGameState == GameState.ModeSelect)
            {
                bool triggerEvent = false;

                lastMouseState = currentMouseState;
                currentMouseState = Mouse.GetState();

                if (lastMouseState.LeftButton == ButtonState.Released && Rect.Contains(currentMouseState.X, currentMouseState.Y) && currentMouseState.LeftButton == ButtonState.Pressed)
                    triggerEvent = true;


                lastTouchState = currentTouchState;
                currentTouchState = TouchPanel.GetState();

                if (currentTouchState.Count >= 1)
                {
                    var currentFirstTouch = currentTouchState.FirstOrDefault();
                    var lastFirstTouch = lastTouchState.FirstOrDefault();

                    if ((lastFirstTouch == null || lastFirstTouch.State == TouchLocationState.Released || lastFirstTouch.State == TouchLocationState.Invalid) && currentFirstTouch != null && Rect.Contains(currentFirstTouch.Position.X, currentFirstTouch.Position.Y) && (currentFirstTouch.State == TouchLocationState.Pressed || currentFirstTouch.State == TouchLocationState.Moved))
                        triggerEvent = true;
                }


                if (triggerEvent)
                {
                    PropertyManager.Initialize();
                    PropertyManager.hasBeenReset = true;

                    PropertyManager.CurrentGameState = GameState.InProgress;
                    PropertyManager.CurrentGameMode = GameMode.ClearTheBoard;

                    TimeManager.GameClock.Restart();
                }
                    //throw new NotImplementedException(); 
            }
        }

    }
}
