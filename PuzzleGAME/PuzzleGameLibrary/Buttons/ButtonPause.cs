﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Configuration;
using PuzzleGameCrossPlatform.Enums;
using PuzzleGameCrossPlatform.Static_Managers;
using Microsoft.Xna.Framework.Input.Touch;

namespace PuzzleGame
{
    public class ButtonPause : ButtonForGames
    {
        private const string _pauseText = "Pause";
        private const string _playText = "Play :)";

        private Texture2D _pauseButtonTexture;
        private Texture2D _playButtonTexture;

        public ButtonPause()
        {
            Initialize();
        }

        public ButtonPause(Rectangle rect, string text)
        {
            _pauseButtonTexture = AssetManager.PauseBtnTexture;
            _playButtonTexture = AssetManager.PlayButtonTexture;

            if (PropertyManager.CurrentGameState == GameState.Paused)
                ButtonTexture = _playButtonTexture;
            else
                ButtonTexture = _pauseButtonTexture;

            Rect = rect;
            displayText = text;
        }

        public override void Initialize()
        {
            //Settings with Image
            //ButtonTexture = AssetManager.PauseBtnTexture;

            _pauseButtonTexture = AssetManager.PauseBtnTexture;
            _playButtonTexture = AssetManager.PlayButtonTexture;

            if (PropertyManager.CurrentGameState == GameState.Paused)
                ButtonTexture = _playButtonTexture;
            else
                ButtonTexture = _pauseButtonTexture;

            var startX = SettingsManager.BtnPause_startX;
            var startY = SettingsManager.BtnPause_startY;
            var width = SettingsManager.BtnPause_width;
            var height = SettingsManager.BtnPause_height;
            var startText = SettingsManager.BtnPause_startText;

            Rect = new Rectangle() { X = startX, Y = startY, Width = width, Height = height };
            displayText = startText;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            var coor = new Vector2(Rect.X, Rect.Y);

            //Drawing without a rect
            // spriteBatch.Draw(AssetManager.ButtonTexture, coor, Color.White);

            if (ButtonTexture != null)
                spriteBatch.Draw(ButtonTexture, Rect, null, Color.White);
            else
            {
                spriteBatch.Draw(AssetManager.ButtonTexture, Rect, null, Color.White);
                spriteBatch.DrawString(AssetManager.Font, displayText, coor, Color.Black);
            }
        }

        public override void Update()
        {
            if (PropertyManager.CurrentGameState != GameState.ModeSelect)
            {
                bool triggerEvent = false;

                lastMouseState = currentMouseState;
                currentMouseState = Mouse.GetState();

                if (lastMouseState.LeftButton == ButtonState.Released && Rect.Contains(currentMouseState.X, currentMouseState.Y) && currentMouseState.LeftButton == ButtonState.Pressed)
                    triggerEvent = true;


                lastTouchState = currentTouchState;
                currentTouchState = TouchPanel.GetState();

                if (currentTouchState.Count >= 1)
                {
                    var currentFirstTouch = currentTouchState.FirstOrDefault();
                    var lastFirstTouch = lastTouchState.FirstOrDefault();




                    if ((lastFirstTouch == null || lastFirstTouch.State == TouchLocationState.Released || lastFirstTouch.State == TouchLocationState.Invalid) && currentFirstTouch != null && Rect.Contains(currentFirstTouch.Position.X, currentFirstTouch.Position.Y) && (currentFirstTouch.State == TouchLocationState.Pressed || currentFirstTouch.State == TouchLocationState.Moved))
                        triggerEvent = true;
                }


                if (triggerEvent)
                {
                    if (PropertyManager.CurrentGameState == GameState.InProgress)
                    {
                        PropertyManager.CurrentGameState = GameState.Paused;
                        TimeManager.GameClock.Stop();
                        displayText = _playText;
                        ButtonTexture = _playButtonTexture;
                    }
                    else
                    {
                        PropertyManager.CurrentGameState = GameState.InProgress;
                        TimeManager.GameClock.Start();
                        displayText = _pauseText;
                        ButtonTexture = _pauseButtonTexture;
                    }
                }
            }
        }

    }
}
