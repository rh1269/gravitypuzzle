﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;

namespace PuzzleGame
{
    public abstract class ButtonForGames
    {
        public Rectangle Rect { get; set; }

        public string displayText { get; set; }

        protected MouseState lastMouseState;
        protected MouseState currentMouseState;

        protected TouchCollection lastTouchState;
        protected TouchCollection currentTouchState;

        public Texture2D ButtonTexture;


        // ButtonReset();
        public ButtonForGames() { }

        public ButtonForGames(Rectangle rect, string text = null, Texture2D buttonTexture = null)
        {
            //Initialize mouse state trackers
            lastMouseState = Mouse.GetState();
            currentMouseState = Mouse.GetState();

            //Initialize Touch
            lastTouchState = TouchPanel.GetState();
            currentTouchState = TouchPanel.GetState();

            Rect = rect;
            displayText = text;

            if(buttonTexture != null)
                ButtonTexture = buttonTexture;
        }

        public abstract void Initialize();

        public abstract void Update();

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            var coor = new Vector2(Rect.X, Rect.Y);

            //Drawing without a rect
            // spriteBatch.Draw(AssetManager.ButtonTexture, coor, Color.White);

            if (ButtonTexture != null)
                spriteBatch.Draw(ButtonTexture, Rect, null, Color.White);
            else
            {
                spriteBatch.Draw(AssetManager.ButtonTexture, Rect, null, Color.White);
                spriteBatch.DrawString(AssetManager.Font, displayText, coor, Color.Black);
            }
        }

    }
}
