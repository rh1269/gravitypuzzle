﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using PuzzleGame;
using PuzzleGameCrossPlatform.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuzzleGameLibrary.GameLogic
{
    [Obsolete("This class is a work in a progress, don't use it until ready.")]
    /// <summary>
    /// Collection of Common Methods that need to be run in the Main Game Class in any deployable project.
    /// </summary>
    public class MainCommon
    {

        /// <summary>
        /// Handles the Audio/Sound during the General Game update method
        /// </summary>
        /// <remarks> LastGameState is update to match CurrentGameState after this method is called in the main loop</remarks>
        public void HandleAudio()
        {
            ////Audio

            ////Pause/Play Audio if there was a Game State change
            //if (PropertyManager.LastGameState != PropertyManager.CurrentGameState)
            //{
            //    //If we just start again - start playing normal music
            //    if (PropertyManager.LastGameState == GameState.ModeSelect && PropertyManager.CurrentGameState == GameState.InProgress)
            //    {
            //        MediaPlayer.Play(AssetManager.HeavySlow);
            //        MediaPlayer.IsRepeating = true;
            //    }
            //    //Otherwise if we just get back into play mode - unpause               
            //    else if (PropertyManager.CurrentGameState == GameState.InProgress)
            //    {
            //        //MediaPlayer.Play(AssetManager.HeavySlow);
            //        //MediaPlayer.IsRepeating = true;
            //        MediaPlayer.Resume();
            //    }
            //    //If we left play mode - go ahead and pause
            //    else
            //        MediaPlayer.Pause();
            //}
            //else if (PropertyManager.CurrentGameState == GameState.InProgress) // If there wasn't a state change and the game is already running
            //{
            //    var currentPlayerDangerState = RuleManager.IsPlayerInDanger(PropertyManager.CurrentGameMode);

            //    //If there was a change in the danger state update the song
            //    if (PropertyManager.LastPlayerInDangerState != currentPlayerDangerState)
            //    {
            //        //Danger
            //        if (currentPlayerDangerState == true)
            //        {
            //            MediaPlayer.Play(AssetManager.LightFast);
            //            MediaPlayer.IsRepeating = true;
            //        }
            //        //Safe
            //        else
            //        {
            //            MediaPlayer.Play(AssetManager.HeavySlow);
            //            MediaPlayer.IsRepeating = true;
            //        }

            //    }

            //}

            throw new NotImplementedException();
        }

        /// <summary>
        /// Handles the Audio/Sound during the General Game update method - USES SoundEffectInstances instead of Songs and the media player.
        /// </summary>
        /// <remarks> LastGameState is update to match CurrentGameState after this method is called in the main loop</remarks>
        public void HandleAudio2()
        {
            //Audio

            //Pause/Play Audio if there was a Game State change
            if (PropertyManager.LastGameState != PropertyManager.CurrentGameState)
            {
                //If we just start again - start playing normal music
                if (PropertyManager.LastGameState == GameState.ModeSelect && PropertyManager.CurrentGameState == GameState.InProgress)
                {
                    //MediaPlayer.Play(AssetManager.HeavySlow);
                    //MediaPlayer.IsRepeating = true;

                    PropertyManager.CurrentSoundEffectInstance.Play();
                }
                //Otherwise if we just get back into play mode - unpause               
                else if (PropertyManager.CurrentGameState == GameState.InProgress)
                {
                    //MediaPlayer.Resume();

                    PropertyManager.CurrentSoundEffectInstance.Play();

                }
                //If we left play mode - go ahead and pause
                else
                    //MediaPlayer.Pause();
                    PropertyManager.CurrentSoundEffectInstance.Stop();

            }
            else if (PropertyManager.CurrentGameState == GameState.InProgress) // If there wasn't a state change and the game is already running
            {
                var currentPlayerDangerState = RuleManager.IsPlayerInDanger(PropertyManager.CurrentGameMode);

                //If there was a change in the danger state update the song
                if (PropertyManager.LastPlayerInDangerState != currentPlayerDangerState)
                {
                    //Danger
                    if (currentPlayerDangerState == true)
                    {
                        //MediaPlayer.Play(AssetManager.LightFast);
                        //MediaPlayer.IsRepeating = true;

                        PropertyManager.CurrentSoundEffectInstance = AssetManager.LightFastInstance;
                        PropertyManager.CurrentSoundEffectInstance.Play();
                        AssetManager.HeavySlowInstance.Stop();

                    }
                    //Safe
                    else
                    {
                        //MediaPlayer.Play(AssetManager.HeavySlow);
                        //MediaPlayer.IsRepeating = true;

                        PropertyManager.CurrentSoundEffectInstance = AssetManager.HeavySlowInstance;
                        PropertyManager.CurrentSoundEffectInstance.Play();
                        AssetManager.LightFastInstance.Stop();
                    }

                }

            }
        }


        /// <summary>
        /// Resets the game -- Should reinitialize everything
        /// </summary>
        public void PuzzleGameReset()
        {
            //Reset Buttons
            //pauseBtn = new ButtonPause();

            //Reset Managers
            PropertyManager.Initialize();
            PropertyManager.hasBeenReset = false;

            //For new timer we must reset
            TimeManager.Initialize(new System.TimeSpan());
            TimeManager.GameClock.Start();

            GameModeInitialization();
        }

        /// <summary>
        /// Populate Game Mode specific initialization variable or settings
        /// </summary>
        private void GameModeInitialization()
        {
            if (PropertyManager.CurrentGameMode == GameMode.ClearTheBoard)
            {
                //For clear the board, kick off witha  new falling row
            }
            else if (PropertyManager.CurrentGameMode == GameMode.RaceTheClock)
            {
                //For clear the board, kick off with a new falling row
                // TimeManager.CountDownTimer = new System.Timers.Timer(SettingsManager.CountDownTimerInitialTime);
            }
        }

        /// <summary>
        /// GameMode specific update tasks
        /// </summary>
        public void GameModeUpdate()
        {
            //The RTC game mode has a count down timer that is managed here
            if (PropertyManager.CurrentGameMode == GameMode.RaceTheClock)
            {
                TimeManager.UpdateCountDownTime();
            }
        }


        //TODO:  Move common logic between PuzzleGame.cs and PuzzleGameAndroid.cs that can be moved (Like Static Manager class interaction, simple functions) into stubbed out methods below
        //----------------------------------------------------------------------------------------------------------------------
        public void LoadContentCommon(GraphicsDevice graphicsDevice, SpriteBatch spriteBatch, GraphicsDeviceManager graphics, ref double HeightScaler, ref double WidthScaler)
        {
            throw new NotImplementedException();
        //    // Create a new SpriteBatch, which can be used to draw textures.
        //    spriteBatch = new SpriteBatch(graphicsDevice);



        //    var targetHeight = 720;
        //    var targetWidth = 1200;

        //    graphics.PreferredBackBufferWidth = targetWidth;
        //    graphics.PreferredBackBufferHeight = targetHeight;

        //    var systemDisplayHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
        //    var systemDisplayWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;

        //    HeightScaler = (systemDisplayHeight / targetHeight);
        //    WidthScaler = (systemDisplayWidth / targetWidth);

        //    //TODO: use this.Content to load your game content here


        //    //MANAGER INIT
        //    //Load these first
        //    AssetManager.Load(Content, graphics.GraphicsDevice);
        //    //Load Asset Manager first
        //    SettingsManager.Initialize(new WindowsSettingsLoader(), HeightScaler, WidthScaler);

        //    PropertyManager.Initialize();
        //    PropertyManager.hasBeenReset = false;



        //    PropertyManager.CurrentGameState = GameState.ModeSelect;
        //    PropertyManager.CurrentGameMode = GameMode.NotAvailable;
        //    // PropertyManager.CurrentGameState = GameState.InProgress;
        //    //PropertyManager.CurrentGameMode = GameMode.RaceTheClock;
        //    PropertyManager.LastGameState = PropertyManager.CurrentGameState;

        //    TimeManager.Initialize(new System.TimeSpan());

        //    //Initialize the ViewManager with the spritebatch object, from here on out ONLY use the View Manager spritebatch
        //    ViewManager.Initialize(spriteBatch);

        //    ////OBJECT INIT
        //    resetBtn = new ButtonReset();
        //    pauseBtn = new ButtonPause();

        //    homeBtn = new ButtonHome();
        //    ctbBtn = new ButtonModeClearTheBoard();
        //    rtcBtn = new ButtonModeRaceTheClock();

        //    //LAST THING TO DO
        //    //GameTimer.Start();
        //    TimeManager.GameClock.Start();

        //    // Populate Game Mode specific initialization
        //    GameModeInitialization();
        }





        public void UnloadContentCommon()
        {
            throw new NotImplementedException();
        }

        public void UpdateCommon()
        {
            throw new NotImplementedException();

        // //if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
        // //       Exit();

        //// TODO: Add your update logic here

        //PropertyManager.GameBoard.Update();

        //    resetBtn.Update();
        //    pauseBtn.Update();
        //    homeBtn.Update();
        //    ctbBtn.Update();
        //    rtcBtn.Update();

        //    if (PropertyManager.hasBeenReset == true)
        //        PuzzleGameReset();

        ////Check Rules in rule manager.  
        //RuleManager.RunRules();

        //    //If we are in "Clear The Board" and we want to attempt speed changes
        //    if (SettingsManager.GlobalAttemptSpeedUp && PropertyManager.CurrentGameMode == GameMode.ClearTheBoard && TimeManager.IsTimeToAttemptSpeedUp())
        //        TimeManager.SpeedUpRepopulation();

        //    //If we are in "Race the clock" and we want to attempt speed changes
        //    if (SettingsManager.GlobalAttemptSpeedUp && PropertyManager.CurrentGameMode == GameMode.RaceTheClock && TimeManager.IsTimeToAttemptSpeedDown())
        //        TimeManager.DecreaseSpeedRepopulation();

        //    //If enough time has passed trickle cells down from the top
        //    if (TimeManager.IsTimeToAttemptRepopulation())
        //        //RuleManager.PopulateTopFillDown();
        //        //RuleManager.PopulateBottomFillUp();
        //        //RuleManager.PopulateLeftFillRight();
        //        //RuleManager.PopulateRightFallToLeft();
        //        RuleManager.RepopulateMissingCells(true);

        //    if (TimeManager.IsTimeToAttemptGravityChange())
        //        PropertyManager.GravityDirection = PropertyManager.getRandomDirection();

        //    if (TimeManager.IsTimeToAttemptComboChange())
        //        PropertyManager.CurrentCombo = new Combo();

        //    //If there are game mode specific updates that need to be made, do them here.
        //    if (PropertyManager.CurrentGameState == GameState.InProgress)
        //        GameModeUpdate();

        ////Audio
        //HandleAudio();

        ////Update Game Global Game State Properties
        //PropertyManager.LastGameState = PropertyManager.CurrentGameState;
        //    PropertyManager.LastPlayerInDangerState = RuleManager.IsPlayerInDanger(PropertyManager.CurrentGameMode);
        }

        public void DrawCommon()
        {
            throw new NotImplementedException();
        }
    }
}
