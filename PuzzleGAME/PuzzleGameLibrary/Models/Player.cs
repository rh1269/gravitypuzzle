﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuzzleGame.Models
{
    public class Player
    {
        //Needed for puzzle Game
        public int score { get; set; }
        
        //Likely Not Needed for puzzle Game
        public int playerNumber { get; set; }
        public bool currentWinner { get; set; }
        public Texture2D playerTexture { get; set; }

        public string name { get; set; }
        public int winCount { get; set; }
      
    }
}
