﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using PuzzleGame;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PuzzleGameLibrary.Models;

namespace PuzzleGameCrossPlatform.Models
{
    public class CellContent
    {

        public CellContent()
        {
          
        }

        public CellContent(CellTypeEnum cellType)
        {
            CellType = cellType;
            PopulateCellContent(CellType);
        }

        public CellContent PopulateWithRandom()
        {
                CellType = GetRandomCellType();
                PopulateCellContent(CellType);
            return this;
        }

        public bool IsEmpty()
        {
            if (CellType == CellTypeEnum.Empty)
                return true;
            return false;
        }

        // public Texture2D _texture;

        public Texture2D CellTexture {get; set; }


        /// <summary>
        /// What type of content is in the cell
        /// </summary>
        public CellTypeEnum CellType{ get;set; }


        /// <summary>
        /// Overall Color of an asset
        /// </summary>
        public Color CellTextureColor
        {
            get
            {
                if (CellType == CellTypeEnum.Green)
                    return Color.Green;
                else if (CellType == CellTypeEnum.Red)
                    return Color.Red;
                else if (CellType == CellTypeEnum.Blue)
                    return Color.Blue;
                else if (CellType == CellTypeEnum.Orange)
                    return Color.Orange;
                else if (CellType == CellTypeEnum.X)
                    return Color.DodgerBlue;
                else if (CellType == CellTypeEnum.O)
                    return Color.HotPink;
                else if (CellType == CellTypeEnum.Square)
                    return Color.Orange;
                else if (CellType == CellTypeEnum.Triangle)
                    return Color.Green;
                else if (CellType == CellTypeEnum.Diamond)
                    return Color.Red;
                else if (CellType == CellTypeEnum.Star)
                    return Color.Yellow;
                else if (CellType == CellTypeEnum.Skull)
                    return Color.Black;
                else
                    return Color.Gray;

            }

        }

        public AnimatedSprite CellSprite { get; set; }

        /// <summary>
        /// Get a Random Cell type between 0 and the passed in type highestNumCellTypes
        /// </summary>
        /// <param name="highestNumCellTypes">The highest cell type to consider for a random selection</param>
        /// <returns></returns>
        private CellTypeEnum GetRandomCellType(int highestNumCellTypes = 5)
        {
            var random = new Random(Guid.NewGuid().GetHashCode());
            var SelectType = (CellTypeEnum)random.Next(1, highestNumCellTypes);
            
            return SelectType;
        }

        private void PopulateCellContent(CellTypeEnum cellType)
        {
            switch (cellType)
            {
                default:
                case CellTypeEnum.Green: CellSprite = new AnimatedSprite(AssetManager.Green, 2, 2); break;
                case CellTypeEnum.Red: CellSprite = new AnimatedSprite(AssetManager.Red, 3, 3); break;
                case CellTypeEnum.Blue: CellSprite = new AnimatedSprite(AssetManager.Blue, 3, 2); break;
                case CellTypeEnum.Orange: CellSprite = new AnimatedSprite(AssetManager.Orange, 4, 4, 15); break;
           
                //Expanded
                case CellTypeEnum.Diamond: CellTexture = AssetManager.Diamond; break;
                case CellTypeEnum.Star: CellTexture = AssetManager.Star; break;
                case CellTypeEnum.Skull: CellTexture = AssetManager.Skull; break;

                //Original Debug
                case CellTypeEnum.X: CellTexture = AssetManager.X; break;
                case CellTypeEnum.O: CellTexture = AssetManager.O; break;
                case CellTypeEnum.Square: CellTexture = AssetManager.Square; break;
                
                case CellTypeEnum.Triangle: CellTexture = AssetManager.Triangle;
                                             break;
            }
        }
    }



}
