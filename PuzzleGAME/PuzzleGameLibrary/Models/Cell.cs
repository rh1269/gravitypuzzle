﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using PuzzleGameCrossPlatform;
using PuzzleGameCrossPlatform.Models;
using PuzzleGameCrossPlatform.Static_Managers;
using PuzzleGameCrossPlatform.Enums;
using Microsoft.Xna.Framework.Input.Touch;
using PuzzleGameLibrary.Explosion;

namespace PuzzleGame.Models
{
    public class Cell
    {
        public int Ypos { get; set; }
        public int Xpos { get; set; }

        public int rowPos { get { return Ypos; } }
        public int colPos { get { return Xpos; } }

        public Rectangle rect { get; set; }

        public CellContent Content { get; set; }
        
        /// <summary>
        /// Custom Background Texture.  Added for using custom background to indicate the combo row.
        /// </summary>
        public Texture2D BackgroundTexture { get; set; }

        public bool updatedRecently { get; set; }

        public int YPixelPosTarget { get; set; }
        public int XPixelPosTarget { get; set; }

        private bool _isSelected { get; set; }
        public bool isSelected
        {
            get { return _isSelected; }
            set
            {
                if (value == true)
                    PropertyManager.SelectedCells.Add(this);
                _isSelected = value;
            }
        }

        public event Action OnClick;
        public event Action OnHover;

        private MouseState lastMouseState;
        private MouseState currentMouseState;

        protected TouchCollection lastTouchState;
        protected TouchCollection currentTouchState;

        private Explosion cellExplosion;



        public Cell()
        {

            lastMouseState = Mouse.GetState();
            currentMouseState = Mouse.GetState();

            //Initialize Touch
            lastTouchState = TouchPanel.GetState();
            currentTouchState = TouchPanel.GetState();

            Content = new CellContent().PopulateWithRandom();
            updatedRecently = false;

        }

        public void Update(MouseState state, TouchCollection touchState)
        {
            //Update the position of the cell rectangle if needed
            if (SettingsManager.GlobalTryAnimation)
                updatePositions();

            //lastMouseState = currentMouseState;
            //currentMouseState = state;

            //if (lastMouseState.LeftButton == ButtonState.Released && rect.Contains(currentMouseState.X, currentMouseState.Y) && currentMouseState.LeftButton == ButtonState.Pressed && /*PropertyManager.GameInProgress ==true*/ PropertyManager.CurrentGameState == GameState.InProgress)
            //{

            bool triggerEvent = false;

            lastMouseState = currentMouseState;
            currentMouseState = state;

            if (lastMouseState.LeftButton == ButtonState.Released && rect.Contains(currentMouseState.X, currentMouseState.Y) && currentMouseState.LeftButton == ButtonState.Pressed)
                triggerEvent = true;


            lastTouchState = currentTouchState;
            currentTouchState = touchState;

            if (currentTouchState.Count >= 1)
            {
                var currentFirstTouch = currentTouchState.FirstOrDefault();
                var lastFirstTouch = lastTouchState.FirstOrDefault();

                if ((lastFirstTouch == null || lastFirstTouch.State == TouchLocationState.Released || lastFirstTouch.State == TouchLocationState.Invalid) && currentFirstTouch != null && rect.Contains(currentFirstTouch.Position.X, currentFirstTouch.Position.Y) && (currentFirstTouch.State == TouchLocationState.Pressed || currentFirstTouch.State == TouchLocationState.Moved))
                    triggerEvent = true;
            }


            if (triggerEvent)
            {

                if (PropertyManager.SelectedCells.Count == 1 && isSelected == true && PropertyManager.SelectedCells.First() == this)
                {
                    isSelected = false;
                    PropertyManager.SelectedCells.Clear();
                }
                else if (!PropertyManager.SelectedCells.Any() || isNextToCell(PropertyManager.SelectedCells))
                {

                    if (!isSelected)
                    {
                        if (Content.CellType != CellTypeEnum.Empty)
                            isSelected = true;
                    }
                    else
                    {
                        RuleManager.EvaluateAndClearSelectedCells();

                    }
                }

            }
        }

        /// <summary>
        /// Draw the current state of the cell
        /// </summary>
        /// <param name="spriteBatch"></param>
        public void Draw(SpriteBatch spriteBatch)
        {
            //If there is an explosion draw it. If theres an explosion but no particles, null out the explosion
            if (cellExplosion != null)
            {
                cellExplosion.Draw(spriteBatch);
                if (cellExplosion.particleList == null)
                {
                    cellExplosion = null;
                }
            }

            //If the cell is part of the combo row it has a special background texture, if it has a background texture draw it.
            if (BackgroundTexture != null)
                spriteBatch.Draw(BackgroundTexture, rect, null, Color.White);

            //If the cell is selected highlight it
            if (isSelected && !Content.IsEmpty())
                spriteBatch.Draw(AssetManager.HighlightTexture, rect, null, Color.White);

            //If the cell has a texture draw it
            if (!Content.IsEmpty())
            {
                if (Content.CellSprite != null)                    
                    Content.CellSprite.Draw(spriteBatch, new Vector2(rect.Location.X, rect.Location.Y), rect.Width, rect.Height);
                else
                    spriteBatch.Draw(Content.CellTexture, rect, null, Color.White);
            }



#if DEBUG
            spriteBatch.DrawString(AssetManager.Font, "(" + Xpos + "," + Ypos + ")", new Vector2(rect.X, rect.Y), Color.Black);

#endif
        }

        //Animation attempt  - Update the position of the cell if needed, runs every cycle
        private void updatePositions()
        {
            updatePositionForDragCell();
        }



#warning TODO

        //Supply target position, store in target position properties(need to make). Update Draw method so that over each cycle we slowly change the x/y pos properties to the target until x/y pos = target x/y pos
        public void MoveToHere()
        {
            throw new NotImplementedException("This method is yet to be implemented, unsure if this is needed at the moment");
        }


        /// <summary>
        /// Assign a different cell to this cells position
        /// </summary>
        /// <param name="cellToDrag"></param>
        public void assignDragCell(Cell cellToDrag)
        {

            rect = new Rectangle(cellToDrag.rect.X, cellToDrag.rect.Y, cellToDrag.rect.Width, cellToDrag.rect.Height);
            Content = cellToDrag.Content;

        }

        /// <summary>
        /// Use this method to clear the content of a cell
        /// </summary>
        public void ClearCell()
        {
            //Initiate Explosions
            var approxCellMidPointOffset = SettingsManager.CellSize / 2;
            var pattern = patternSelector(PropertyManager.SelectedCells.Count);
            cellExplosion = new Explosion(new Vector2(rect.X + approxCellMidPointOffset, rect.Y + approxCellMidPointOffset), Content.CellTextureColor, pattern);

            //Clear old Cell Content
            Content = new CellContent();           
        }

        private void updatePositionForDragCell()
        {
            //Default to the current positions
            int newX = rect.X;
            int newY = rect.Y;

            //For X
            if (rect.X < XPixelPosTarget)
                // newX= rect.X + SettingsManager.CellSpeedPixelPerCycle;
                newX = getNeededAmountToFinishMove(rect.X, XPixelPosTarget, true, SettingsManager.CellSpeedPixelPerCycle);
            if (rect.X > XPixelPosTarget)
                //newX = rect.X - SettingsManager.CellSpeedPixelPerCycle;
                newX = getNeededAmountToFinishMove(rect.X, XPixelPosTarget, false, SettingsManager.CellSpeedPixelPerCycle);

            //For Y
            if (rect.Y < YPixelPosTarget)
                //newY = rect.Y + SettingsManager.CellSpeedPixelPerCycle;
                newY = getNeededAmountToFinishMove(rect.Y, YPixelPosTarget, true, SettingsManager.CellSpeedPixelPerCycle);
            if (rect.Y > YPixelPosTarget)
                // newY = rect.Y - SettingsManager.CellSpeedPixelPerCycle;
                newY = getNeededAmountToFinishMove(rect.Y, YPixelPosTarget, false, SettingsManager.CellSpeedPixelPerCycle);

            if (rect.X != newX || rect.Y != newY)
                rect = new Rectangle(newX, newY, SettingsManager.CellSize, SettingsManager.CellSize);

        }

        //Handle if speed causes update to overshoot positions (may need to refactor to use mod to determine appropriate speeds ~ if refactoring we may want to determine this in the settings provider instead and move by an exact speed)
        private int getNeededAmountToFinishMove(int currentPos, int targetPos, bool isAdd, int pixelPerCycle)
        {
            int newPos = currentPos;

            if (isAdd)
            {
                newPos = currentPos + pixelPerCycle;
                if (newPos > targetPos)
                    newPos = targetPos;
            }
            if (!isAdd)
            {
                newPos = currentPos - pixelPerCycle;
                if (newPos < targetPos)
                    newPos = targetPos;
            }

            return newPos;
        }

        //DEPENDENT on static gameboard
        //Am I- the current cell - next to a cell in the selected cells list
        ///TODO- I oversolved this originally - change this to care about coordinates instead of object comparisons       
        private bool isNextToCell(List<Cell> selectedCells)
        {
            Cell above = (Ypos == 0) ? null : PropertyManager.GameBoard.Grid[Xpos, Ypos - 1];
            Cell below = (Ypos + 1 == PropertyManager.GameBoard.Ymax) ? null : PropertyManager.GameBoard.Grid[Xpos, Ypos + 1];
            Cell left = (Xpos == 0) ? null : PropertyManager.GameBoard.Grid[Xpos - 1, Ypos];
            Cell right = (Xpos + 1 == PropertyManager.GameBoard.Xmax) ? null : PropertyManager.GameBoard.Grid[Xpos + 1, Ypos];

            foreach (var cell in selectedCells)
            {

                if (cell == above || cell == below || cell == left || cell == right)
                    return true;
            }

            //If we allow Diagonals to work, then check for a potential diagonal match
            if (SettingsManager.GlobalAllowDiagonalSelection)
            {
                //var myX = Xpos;
                //var myY = Ypos;
                foreach (var cell in selectedCells)
                {
                    //above left check                    
                    if (cell.Xpos == Xpos - 1 && cell.Ypos == Ypos - 1)
                        return true;

                    //above right check
                    if (cell.Xpos == Xpos + 1 && cell.Ypos == Ypos - 1)
                        return true;

                    //down left check
                    if (cell.Xpos == Xpos - 1 && cell.Ypos == Ypos + 1)
                        return true;

                    //down right check
                    if (cell.Xpos == Xpos + 1 && cell.Ypos == Ypos + 1)
                        return true;
                }
            }

            return false;

        }

        //Determine thresholds for displaying different patterns
        private ExplosionPattern patternSelector(int numOfSelectedCells)
        {
            if (numOfSelectedCells > 4)
                return ExplosionPattern.Star;
            if (numOfSelectedCells > 2)
                return ExplosionPattern.X;

            return ExplosionPattern.Cross;

        }

    }

}
