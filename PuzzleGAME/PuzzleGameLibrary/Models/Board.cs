﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Configuration;
using PuzzleGameCrossPlatform.Static_Managers;
using PuzzleGameCrossPlatform.Enums;
using PuzzleGameCrossPlatform.Models;
using Microsoft.Xna.Framework.Input.Touch;

namespace PuzzleGame.Models
{
    /// <summary>
    /// This class is a representation of the main gameboard/grid - a 2d array
    /// </summary>
    public class Board
    {
        public Cell[,] Grid;

        public int Xmax;
        public int Ymax;
        public int rectSize;

        public int GameBoardStartingX;
        public int GameBoardStartingY;

        public Rectangle BoardBackgroundRectangle;

        public Board()
        {
            Xmax = SettingsManager.Board_Xmax;
            Ymax = SettingsManager.Board_Ymax;

            rectSize = SettingsManager.CellSize;

            GameBoardStartingX = SettingsManager.GameBoardStartingX;
            GameBoardStartingY = SettingsManager.GameBoardStartingY;

            BoardBackgroundRectangle = new Rectangle(GameBoardStartingX, GameBoardStartingY, Xmax * rectSize, Ymax * rectSize);

            initGrid();
        }

        public void Update()
        {
            for (int x = 0; x < Xmax; x++)
                for (int y = 0; y < Ymax; y++)
                {
                    Grid[x, y].Update(Mouse.GetState(), TouchPanel.GetState());
                }
        }

        public void Draw(SpriteBatch sb)
        {
             sb.Draw(AssetManager.BoardBackgroundColor, BoardBackgroundRectangle, null, Color.White);

            for (int x = 0; x < Xmax; x++)
                for (int y = 0; y < Ymax; y++)
                {
                    Grid[x, y].Draw(sb);
                }
        }

        private void initGrid()
        {
            if (Grid != null)
                return;

            //Instantiate Grid
            Grid = new Cell[Xmax, Ymax];

            //Fill Grid with cells
            int startx = GameBoardStartingX;
            int starty = GameBoardStartingY;
            for (int x = 0; x < Xmax; x++)
            {
                starty = GameBoardStartingY;
                for (int y = 0; y < Ymax; y++)
                {
                    Grid[x, y] = new Cell()
                    {
                        //position in grid
                        Xpos = x,
                        Ypos = y,

                        //pixel position
                        rect = new Rectangle(startx, starty, rectSize, rectSize),

                        XPixelPosTarget = startx,
                        YPixelPosTarget = starty
                    };

                    starty += rectSize;
                }

                startx += rectSize;
            }

            //Start with empty cells, if the board is full it is game over
            if (PropertyManager.CurrentGameMode == GameMode.ClearTheBoard)
            {
                for (int x = 0; x < Xmax; x++)
                {
                    for (int y = 0; y < Ymax; y++)
                    {
                        //Clear the content of all cells, except last row
                        if (y < Ymax - 1)
                            Grid[x, y].Content = new CellContent();
                    }
                }
            }

        }

        public bool NeedsToBeEvaluated()
        {
            foreach (var cell in Grid)
            {
                if (cell.updatedRecently == true)
                {
                    cell.updatedRecently = false;
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Returns the number of populated cells on the board
        /// </summary>
        /// <returns></returns>
        public int GetNumberOfPopulatedCells()
        {
            int numberOfCellsWithContent = 0;

            foreach (var c in Grid)
            {
                if (!c.Content.IsEmpty())
                    numberOfCellsWithContent++;
            }

            return numberOfCellsWithContent;
        }

    }
}
