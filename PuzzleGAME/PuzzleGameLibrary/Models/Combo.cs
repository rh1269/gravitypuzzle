﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PuzzleGame;
using PuzzleGame.Models;
using PuzzleGameCrossPlatform.Static_Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuzzleGameCrossPlatform.Models
{
    public class Combo
    {
        public Cell[] ComboArray;
                
        public bool isHorizontal;
        
                
        public Combo()
        {
            ComboArray = new Cell[SettingsManager.ComboSize];
            isHorizontal = SettingsManager.ComboHorizontal;

            InitCombo();
        }

        public void Draw(SpriteBatch sb)
        {
            for (int iterator = 0; iterator < SettingsManager.ComboSize; iterator++)
            {
                ComboArray[iterator].Draw(sb);
            }

        }

        public void Reset()
        {
            for (int x = 0; x < SettingsManager.ComboSize; x++)
            {
                ComboArray[x].Content = new CellContent().PopulateWithRandom();
            }
        }

        public CellTypeEnum[] DeepCopyCellContent()
        {
            var newComboArray = new CellTypeEnum[SettingsManager.ComboSize];

            for (int x = 0; x < SettingsManager.ComboSize; x++)
            {
                newComboArray[x] = ComboArray[x].Content.CellType ;
            }

            return newComboArray;
        }
        private void InitCombo()
        {
           
            int startx = SettingsManager.ComboStartingX;
            int starty = SettingsManager.ComboStartingY;

            for (int iterator = 0; iterator < SettingsManager.ComboSize; iterator++)
            {
                ComboArray[iterator] = new Cell();

                ComboArray[iterator].Content = new CellContent().PopulateWithRandom();

                ComboArray[iterator].BackgroundTexture = AssetManager.specialComboBackgroundTexture;
                
                ComboArray[iterator].rect = new Rectangle(startx, starty, SettingsManager.CellSize, SettingsManager.CellSize);

                if (isHorizontal)
                {
                    ComboArray[iterator].Xpos = iterator;
                    ComboArray[iterator].Ypos = 0;
                    startx += SettingsManager.CellSize;
                }
                else
                {
                    ComboArray[iterator].Xpos = 0;
                    ComboArray[iterator].Ypos = iterator;
                    starty += SettingsManager.CellSize;
                }
                

            }
            
        }

    }
}
