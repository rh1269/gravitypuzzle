﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PuzzleGame;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuzzleGameLibrary.Models
{
    //This class is based off of a modified version of the tutorial found here:
    //http://rbwhitaker.wikidot.com/monogame-texture-atlases-2
    public class AnimatedSprite
    {
        public Texture2D SpriteSheet;

        public int SpriteSheetRows;
        public int SpriteSheetCols;

        private int currentFrame;
        private int totalFrames;

        private int _singleSpriteWidth;
        private int _singleSpriteHeight;

        private TimeSpan _lastTimeFrameSwitch;
        private TimeSpan _timeNeededForFrameChange;
        
        public AnimatedSprite(Texture2D spriteSheet, int rows, int columns, int? total = null, TimeSpan? timeBetweenFrames = null)
        {
            SpriteSheet = spriteSheet;
            SpriteSheetRows = rows;
            SpriteSheetCols = columns;
            currentFrame = 0;
            totalFrames = (total != null) ? Convert.ToInt32(total) : rows * columns;

            _singleSpriteWidth = spriteSheet.Width / columns;
            _singleSpriteHeight = spriteSheet.Height / rows;

            _lastTimeFrameSwitch = new TimeSpan(0, 0, 0, 0); ;

            if (timeBetweenFrames != null)
                _timeNeededForFrameChange = (TimeSpan)timeBetweenFrames;
            else
                _timeNeededForFrameChange = new TimeSpan(0, 0, 0, 0, 250);

        }

        public void Update() {

            var currentTime = TimeManager.GameClock.Elapsed;

            if (currentTime > _lastTimeFrameSwitch + _timeNeededForFrameChange)
            {
                currentFrame++;
                _lastTimeFrameSwitch = currentTime;
            }

            if (currentFrame >= totalFrames)
                currentFrame = 0;
        }

        /// <summary>
        /// Draw the current Frame in the sprite sheet, the rectangle will match the size of the sub texture
        /// </summary>
        /// <param name="sb">Sprite batch</param>
        /// <param name="location">Position to draw the rectangle</param>
        public void Draw(SpriteBatch sb, Vector2 location)
        {
            Draw(sb, location, _singleSpriteWidth, _singleSpriteHeight);
        }

        /// <summary>
        /// Draw the current Frame in the sprite sheet, the rectangle will be the size specified
        /// </summary>
        /// <param name="sb">Sprite batch</param>
        /// <param name="location">Position to draw the rectangle</param>
        /// <param name="rectWidth">New rectanlge width</param>
        /// <param name="rectHeight">New rectangle height</param>
        public void Draw(SpriteBatch sb, Vector2 location, int rectWidth, int rectHeight)
        {
            //Get the Sprite Sheet Row and Col we want
            int spriteSheetRow = (int)((float)currentFrame / (float)SpriteSheetCols);
            int spriteSheetCol = currentFrame % SpriteSheetCols;

            //This rectangle will be a "slice" of the sprite sheet that gives us the current sprite/sub-texture we want for the current frame
            Rectangle sourceRec = new Rectangle(_singleSpriteWidth * spriteSheetCol, _singleSpriteHeight * spriteSheetRow, _singleSpriteWidth, _singleSpriteHeight);

            //This is where we want to put the slice, we will send the slice to this position;
            Rectangle destinationRec = new Rectangle((int)location.X, (int)location.Y, rectWidth, rectHeight);


            //sb.Begin();
            sb.Draw(SpriteSheet, destinationRec, sourceRec, Color.White);
            //sb.End();
            Update();
        }
    }
}
