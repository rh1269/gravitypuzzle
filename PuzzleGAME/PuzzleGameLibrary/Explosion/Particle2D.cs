﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using PuzzleGame;
using PuzzleGameCrossPlatform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuzzleGameLibrary.Explosion
{
    //Combination of a point in 2D space, direction, speed
    public class Particle2D
    {
       

        /// <summary>
        /// Pixel Per Second
        /// </summary>
        public int Speed { get; set; }

        public DirectionEnum CurrentDirection { get; set; }

        public Color ParticleColor { get; set; }

        public int DistanceToTravel { get; set; }
        
        private int _distanceTravelled { get; set; }


        /// <summary>
        /// These controls determine how often to update
        /// </summary>
        //Update every X times through
        public int Skip { get; set; }
        //The current place through the skip cycle
        private int _currentSkip { get; set; }



        public Rectangle rect { get; set; }
        public int PixelPositionX { get { return rect.X; } }
        public int PixelPositionY { get { return rect.Y; } }
        public Texture2D ParticleContent { get; set; }

        



        //public Particle2D()
        //{
        //    Speed = 10;
        //    CurrentDirection = DirectionEnum.Right;
        //    rect = new Rectangle(200, 200,10, 10);
        //    ParticleColor = Color.Red;
        //    DistanceToTravel = 150;

        //    //
        //    Skip = 0;
        //    _currentSkip = 0;
           
        //}


        public Particle2D(int speed, DirectionEnum direction, Vector2 originPoint, int particleSize, int distanceToTravel, Color particleColor, Texture2D textureOverride = null, int skip = 0)
        {
            Speed = speed;
            CurrentDirection = direction;
            rect = new Rectangle((int)originPoint.X, (int)originPoint.Y, particleSize, particleSize);
            DistanceToTravel = distanceToTravel;

            ParticleColor = particleColor;
            ParticleContent = textureOverride;
            Skip = skip;            

            _currentSkip = 0;
            _distanceTravelled = 0;
        }


        public void Update(MouseState state, TouchCollection touchState)
        {
            throw new NotImplementedException();

        }

        public void Draw(SpriteBatch spriteBatch)
        {
            //throw new NotImplementedException();

            if (ContinueTravelling())
            {
                if (ParticleContent == null)
                {
                    ParticleContent = new Texture2D(spriteBatch.GraphicsDevice, 1, 1);
                    ParticleContent.SetData(new Color[] { ParticleColor });
                }

                spriteBatch.Draw(ParticleContent, rect, null, Color.White);
                
                //Control logic that may need to be handled in Update() method above ^
                if (GoodTurnToUpdate())
                    UpdatePositionBySpeedDirection();

            }  

        }

        
        public void Kill() {
            ParticleContent.Dispose();
        }

        public bool ContinueTravelling()
        {
            if (_distanceTravelled >= DistanceToTravel)
                return false;
            return true;
        }

        private bool GoodTurnToUpdate()
        {
            if (_currentSkip >= Skip)
            {
                _currentSkip = 0;
                return true;
            }
            _currentSkip++;
            return false;
        }

        private void UpdatePositionBySpeedDirection()
        {
            var curX = rect.X;
            var curY = rect.Y;
            var curWidth = rect.Width;
            var curHeight = rect.Height;

            switch (CurrentDirection)
            {
                    case DirectionEnum.Up:                    rect = new Rectangle(curX, curY - Speed, curWidth, curHeight);
                                                          break;
                    case DirectionEnum.Down:                    rect = new Rectangle(curX, curY + Speed, curWidth, curHeight);
                                                          break;
                    case DirectionEnum.Left:                    rect = new Rectangle(curX - Speed, curY, curWidth, curHeight);
                                                          break;
                    case DirectionEnum.Right:                    rect = new Rectangle(curX + Speed, curY, curWidth, curHeight);
                                                          break;
                    case DirectionEnum.UpLeft:                    rect = new Rectangle(curX - Speed, curY + Speed, curWidth, curHeight);
                                                          break;
                    case DirectionEnum.UpRight:                    rect = new Rectangle(curX + Speed, curY + Speed, curWidth, curHeight);
                                                          break;
                    case DirectionEnum.DownLeft:                    rect = new Rectangle(curX - Speed, curY - Speed, curWidth, curHeight);
                                                          break;
                    case DirectionEnum.DownRight:                    rect = new Rectangle(curX + Speed, curY - Speed, curWidth, curHeight);
                                                          break;
                  
             
            }

            _distanceTravelled += Speed;

        }
    }
}
