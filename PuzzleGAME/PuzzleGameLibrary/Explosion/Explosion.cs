﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using PuzzleGameCrossPlatform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PuzzleGameCrossPlatform.Static_Managers;

namespace PuzzleGameLibrary.Explosion
{
    public class Explosion
    {

        public List<Particle2D> particleList { get; set; }

        private int _defaultParticleSpeed { get; set; }
        private int _defaultParticleSize { get; set; }
        private int _defaultParticleDistanceToTravel { get; set;} 

        //Use if SettingsManager is available
        public Explosion(Vector2 originPoint, Color explosionColor, ExplosionPattern pattern, Texture2D particleTexture=null)
        {
            _defaultParticleSpeed = SettingsManager.Explosion_defaultParticleSpeed;
            _defaultParticleSize = SettingsManager.Explosion_defaultParticleSize;
            _defaultParticleDistanceToTravel = SettingsManager.Explosion_defaultParticleDistanceToTravel;

            createExplosion(originPoint, explosionColor, pattern, particleTexture);
        }

        public void Update(MouseState state, TouchCollection touchState)
        {
            throw new NotImplementedException();
        }

        public void Draw(SpriteBatch spriteBatch)
        {

            if (particleList != null)
            {
                foreach (var particle in particleList)
                {
                    particle.Draw(spriteBatch);
                }

                bool timeToClear = true;
                foreach (var p in particleList)
                {
                    if (p.ContinueTravelling())
                        timeToClear = false;
                }

                if (timeToClear)
                {
                    //foreach (var p in particleList)
                    //{
                    //    p.Kill();
                    //}
                    particleList = null;
                    // particleList = new List<Particle2D>();
                    // GC.Collect();
                    // GC.WaitForPendingFinalizers();
                }
            }
         }

        private void createExplosion(Vector2 originPoint, Color explosionColor, ExplosionPattern pattern, Texture2D particleTexture = null)
        {

            switch (pattern) {


                case ExplosionPattern.Cross:  // Cross
                                              particleList = new List<Particle2D>()
                                              {
                                                  new Particle2D(1, DirectionEnum.Up,originPoint, 10, 150, explosionColor, particleTexture),
                                                  new Particle2D(1, DirectionEnum.Down, originPoint, 10, 150, explosionColor, particleTexture),
                                                  new Particle2D(1, DirectionEnum.Left, originPoint, 10,  150, explosionColor, particleTexture),
                                                  new Particle2D(1,DirectionEnum.Right,originPoint,10,150, explosionColor, particleTexture)
                                              };
                                              break;
                case ExplosionPattern.X:     //X
                                              particleList = new List<Particle2D>()
                                              {
                                                  new Particle2D(1, DirectionEnum.UpLeft,originPoint, 10, 150, explosionColor, particleTexture),
                                                  new Particle2D(1, DirectionEnum.UpRight, originPoint, 10, 150, explosionColor, particleTexture),
                                                  new Particle2D(1, DirectionEnum.DownLeft, originPoint, 10,  150, explosionColor, particleTexture),
                                                  new Particle2D(1,DirectionEnum.DownRight,originPoint,10,150, explosionColor, particleTexture)
                                              };
                                             break;

                case ExplosionPattern.Star:     //*
                                              particleList = new List<Particle2D>()
                                              {
                                                  new Particle2D(1, DirectionEnum.Up,originPoint, 10, 150, explosionColor, particleTexture),
                                                  new Particle2D(1, DirectionEnum.Down, originPoint, 10, 150, explosionColor, particleTexture),
                                                  new Particle2D(1, DirectionEnum.Left, originPoint, 10,  150, explosionColor, particleTexture),
                                                  new Particle2D(1,DirectionEnum.Right,originPoint,10,150, explosionColor, particleTexture),

                                                  new Particle2D(1, DirectionEnum.UpLeft,originPoint, 10, 150, explosionColor, particleTexture),
                                                  new Particle2D(1, DirectionEnum.UpRight, originPoint, 10, 150, explosionColor, particleTexture),
                                                  new Particle2D(1, DirectionEnum.DownLeft, originPoint, 10,  150, explosionColor, particleTexture),
                                                  new Particle2D(1,DirectionEnum.DownRight,originPoint,10,150, explosionColor, particleTexture)
                                              };
                                                break;


            }

        }
        

    }


    public enum ExplosionPattern
    {
        Cross = 1,
        X = 2,
        Star = 3,
     //   RandomPattern = 4    
    }
  

}
